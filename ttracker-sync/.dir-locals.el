((nil . ((eval . (set (make-local-variable 'my-project-path)
                      (file-name-directory
                       (let ((d (dir-locals-find-file ".")))
                         (if (stringp d) d (car d))))))
         (eval . (set (make-local-variable 'my-compile-command)
                      (format "cd %s && dub" my-project-path)))
         (compile-command (lambda ()
                            (format "cd %s && dub" my-project-path))))))
