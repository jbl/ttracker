// TTracker
// Copyright (C) 2016  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.sync.rest;

import std.stdio;

import vibe.http.server;
import vibe.web.web;

private import ttracker.lib;

class SyncApiImplementation : SyncApi {
  TTrackerDb db;

  this(TTrackerDb db) {
    this.db = db;
  }


  @property Activity activity(string uuid) {
    return db.getActivityByUuid(uuid);
  }

  @property void activity(Activity a) {
    Activity existing;
    try {
      writeln(a.uuid);
      existing = db.getActivityByUuid(a.uuid);
      db.updateActivity(a);
    } catch (Exception e) {
      writeln(e);
      db.addActivity(a);
    }
  }

  @property Activity[] activities(size_t since) {
    Activity[] res;
    foreach(ref a; db.getActivities()) {
      res ~= a;
    }
    return res;
  }
}
