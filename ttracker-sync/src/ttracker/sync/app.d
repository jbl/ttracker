// TTracker
// Copyright (C) 2016  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.sync.app;

private import ttracker.lib;
private import ttracker.sync.webapp;
private import ttracker.sync.rest;

import vibe.core.core;
import vibe.core.log;
import vibe.d;
import vibe.http.router;
import vibe.http.server;
import vibe.web.rest;

import std.array;
import std.path;
import std.process;
import std.stdio;

shared static this()
{
  auto router = new URLRouter;
  auto config_dir = get_user_config_subdir("ttracker");
  string db_file = buildPath(config_dir, "ttracker_server.sqlite");

  auto db = TTrackerDb(db_file);

  bool checkPassword(string username, string password) {
    User[] users;
    users = db.getUsers(username);
    return users.length > 0 && users[0].username == username && users[0].password_hash == password;
  }

  router.any("*", performBasicAuth("Site Realm", toDelegate(&checkPassword)));
  router.registerRestInterface!SyncApi(new SyncApiImplementation(db), "/ttracker/api/");
  WebInterfaceSettings webSettings = new WebInterfaceSettings;
  webSettings.urlPrefix = "/ttracker/";
  router.registerWebInterface(new WebApp(db), webSettings);
  router.get("*", serveStaticFiles("public/"));

  auto settings = new HTTPServerSettings;
  settings.port = 8080;
  settings.sessionStore = new MemorySessionStore;
  settings.bindAddresses = ["0.0.0.0"];
  listenHTTP(settings, router);
}
