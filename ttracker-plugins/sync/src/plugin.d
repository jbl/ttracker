// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module sync.plugin;

import std.stdio;
import std.conv;

import gtk.AboutDialog;
import gtk.Button;
import gtk.Builder;
import gtk.Entry;

import requests;

public import ttracker.lib.plugins;
import ttracker.lib.models;

class SyncPlugin : ISyncPlugin
{
    Pixbuf icon;

    this()
    {
        icon = get_pixbuf_from_data(cast(immutable ubyte[]) import("ttracker-sync.png"));
    }

    override immutable(string) get_name()
    {
        return "ttracker-sync-plugin";
    }

    override void about(Window parent)
    {
        with (new AboutDialog())
        {
            string[] names;
            names ~= "Jean-Baptiste Lab (jeanbaptiste.lab@gmail.com)";
            setProgramName("TTracker sync plugin");
            setAuthors(names);
            setComments("Sync to TTracker server plugin");
            setCopyright("© Jean-Baptiste Lab (jeanbaptiste.lab@gmail.com)");
            setLicenseType(GtkLicense.MIT_X11);
            setVersion("0.1");
            setLogo(icon);
            setTransientFor(parent);
            setWebsite("https://bitbucket.org/jbl/ttracker");
            setWebsiteLabel("TTracker's page on BitBucket");
            run();
            hide();
        }
    }

    override bool has_preferences()
    {
        return true;
    }

    override void preferences(Window parent, IPluginPreferences prefs)
    {
        auto builder = new Builder();
        builder.addFromString(import("sync_preferences.glade"));
        auto window = cast(Window) builder.getObject("sync_preferences");
        window.setIcon(icon);
        auto ok = cast(Button) builder.getObject("ok");
        auto cancel = cast(Button) builder.getObject("cancel");
        auto username = cast(Entry) builder.getObject("username");
        auto password = cast(Entry) builder.getObject("password");
        auto server = cast(Entry) builder.getObject("server");
        auto plugin_prefs = prefs.plugin_preferences();
        if ("username" !in plugin_prefs)
        {
            username.setText("<not set>");
        }
        else
        {
            username.setText(plugin_prefs["username"].str());
        }
        if ("password" !in plugin_prefs)
        {
            password.setText("<not set>");
        }
        else
        {
            password.setText(plugin_prefs["password"].str());
        }
        if ("server" !in plugin_prefs)
        {
            server.setText("<not set>");
        }
        else
        {
            server.setText(plugin_prefs["server"].str());
        }
        ok.addOnClicked(delegate(Button) {
            plugin_prefs["username"] = username.getText();
            plugin_prefs["password"] = password.getText();
            plugin_prefs["server"] = server.getText();
            prefs.plugin_preferences(plugin_prefs);
            window.hide();
        });
        cancel.addOnClicked(delegate(Button) { window.hide(); });
        window.setTransientFor(parent);
        window.setModal(true);
        window.showAll();
    }

    bool login(string username, string password)
    {
        string[string] params;
        params["username"] = username;
        params["password"] = password;
        auto response = parseJSON(to!string(postContent("http://127.0.0.1:8000/api-token-auth/", params)));
        return cast(bool)("token" in response);
    }

    override void sync(ITTrackerDb db, IPluginPreferences preferences)
    {
        import std.json : parseJSON;

        auto prefs = preferences.plugin_preferences;
        stdout.flush();

        if ("token" !in prefs)
        {
            auto response = parseJSON(to!string(postContent(prefs["server"].str() ~ "/api-token-auth/",
                    ["username": prefs["username"].str(),
                     "password": prefs["password"].str()])));
            if ("token" in response)
            {
                prefs["token"] = response["token"];
            }
        }
        if ("token" in prefs)
        {
            try
            {

                JSONValue activities;
                Activity[] synced;
                activities.array = [];
                foreach (ref a; db.getActivities().filter("changed=1"))
                {
                    activities.array ~= a.asJSON();
                    synced ~= a;
                }
                if (activities.array.length > 0)
                {
                    foreach(ref a; synced) {
                        auto req = Request();
                        req.verbosity = 0;
                        writeln("Authorization Token " ~ prefs["token"].str());
                        req.addHeaders(["Authorization" : "Token " ~ prefs["token"].str()]);
                        Response resp;
                        JSONValue v = asJSON(a);
                        if (a.synced == 0) {
                            writeln("POSTing new activity...");
                            resp = req.exec!"POST"("http://127.0.0.1:8000/api/activities/", toJSON(v), "application/json");
                            auto created = parseJSON(to!string(resp.responseBody));
                            a.remote_id = created["pk"].integer;
                            v = asJSON(a);
                        } else {
                            writeln("PATCHing existing activity " ~ v.object["uuid"].str());
                            v.object.remove("uuid");
                            resp = req.exec!"PATCH"("http://127.0.0.1:8000/api/activities/"~to!string(a.remote_id)~"/", toJSON(v), "application/json");
                            writeln(resp.responseBody);
                            auto updated = parseJSON(to!string(resp.responseBody));
                        }
                        a.synced = 1;
                        a.changed = 0;
                        db.updateActivity(a);
                    }
                }
            }
            catch (Exception e)
            {
                // TODO: log something here instead of swallowing the exception
                writeln(e);
            }
            preferences.plugin_preferences = prefs;
        }
    }
}

class PluginDescriptor : IPluginDescriptor
{
    private
    {
        SyncPlugin instance;
        const(PluginDescription) DESCRIPTOR = {"TTracker Sync", "sync"};
    }

    PluginDescription get_description() const
    {
        return DESCRIPTOR;
    }

    IPlugin get_instance()
    {
        if (instance is null)
        {
            instance = new SyncPlugin();
        }
        return instance;
    }
}

private const(PluginDescriptor) descriptor = new PluginDescriptor();
extern (C) const(IPluginDescriptor) get_descriptor()
{
    return descriptor;
}

unittest
{
    auto p = new SyncPlugin();
}

version (plugins_as_dll)
{
}
else
{
    import ttracker.lib.builtin_plugins : builtin_plugins,
        register_static_plugin;

    static this()
    {
        register_static_plugin(new PluginDescriptor());
    }
}
