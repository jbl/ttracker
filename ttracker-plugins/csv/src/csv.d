// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.plugins.csv;

private import std.algorithm;
private import std.array;
private import std.conv;
private import std.datetime;
private import std.file;
private import std.format;
private import std.json;
private import std.stdio;

import gtk.AboutDialog;
import gtk.Button;
import gtk.Builder;
import gtk.Entry;

public import ttracker.lib.plugins;
public import ttracker.lib.utils;

class CSVReporter : IReportPlugin {
    Pixbuf icon;

    this()
    {
        icon = get_pixbuf_from_data(cast(immutable ubyte[]) import("ttracker-csv.png"));
    }

  override immutable(string) get_name() {
    return "CSV";
  }

  override void about(Window parent) {
    with (new AboutDialog()) {
      string[] names;
      names ~= "Jean-Baptiste Lab (jeanbaptiste.lab@gmail.com)";
      setAuthors(names);
      setComments("TTracker CSV Exporter plugin");
      setCopyright("© Jean-Baptiste Lab (jeanbaptiste.lab@gmail.com)");
      setLicenseType(GtkLicense.MIT_X11);
      setVersion("0.1");
      setLogo(icon);
      setTransientFor(parent);
      setWebsite("https://bitbucket.org/jbl/ttracker");
      setWebsiteLabel("TTracker's page on BitBucket");
      run();
      hide();
    }
  }

  override bool has_preferences() {
    return false;
  }

  override void preferences(Window parent, IPluginPreferences prefs) {
  }

  override void report(ref ITTrackerDb db, string output, Date start, Date end) {
    if (!output.endsWith(".csv")) {
      output ~= ".csv";
    }
    auto output_file = File(output, "w");
    auto start_str = to!string((cast(SysTime)(start)).toUnixTime());
    auto end_str = to!string((cast(SysTime)(end)).toUnixTime());
    auto selection =  "start >= " ~ start_str ~ " and start <= " ~ end_str;

    foreach(ref a; db.getActivities().filter(selection).orderBy("start asc")) {
      string []data;
      data ~= a.name;
      data ~= a.description;
      data ~= to!string(cast(Date)(SysTime.fromUnixTime(a.start)));
      if (a.end != 0) {
        data ~= format("%.1f", (a.end - a.start) / 3600.0);
      } else {
        data ~= "-";
      }
      output_file.writeln(join(data, ";"));
    }
    output_file.close();
  }
}

class PluginDescriptor : IPluginDescriptor {
    private {
        CSVReporter instance;
        const (PluginDescription) DESCRIPTOR = {"CSV Reporter", "report"};
    }

    PluginDescription get_description() const {
        return DESCRIPTOR;
    }
    IPlugin get_instance() {
        if(instance is null) {
            instance = new CSVReporter();
        }
        return instance;
    }
}

private const PluginDescriptor descriptor = new PluginDescriptor();

version(plugins_as_dll) {
    extern (C) const(IPluginDescriptor) get_descriptor() {
        return descriptor;
    }
} else {
    import ttracker.lib.builtin_plugins : builtin_plugins, register_static_plugin;
    static this() {
        register_static_plugin(new PluginDescriptor());
    }
}
