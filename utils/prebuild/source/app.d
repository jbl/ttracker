import std.stdio : File, writeln, stdout;
import std.process : environment;
import std.array : replace, split;
import std.file : dirEntries, SpanMode;
import std.algorithm : map, startsWith;
import std.path : stripExtension;
import std.regex : regex, replaceAll, Captures;
import std.conv : to;

import scriptlike.only;
import docopt;

/**
 * Expand occurences of variables in input
 */
string expand(string input, ref string[string]env) {
	string subst(Captures!(string) m) {
		if (m[1] in env) {
			return env[m[1]];
		} else if (environment.get(m[1])) {
			return environment.get(m[1]);
		}
		return m.hit;
	}
	auto varRE = regex(r"\$\{([\w_]+)\}", "g");
	string result = replaceAll!(subst)(input, varRE);
	if (result == input) {
		return result;
	} else {
		return expand(result, env);
	}
}

unittest {
	string data = "${test} is great (${toto})!";
	string[string] env;
	env["test"] = "JB";
	env["toto"] = "${test}";
	assert(expand(data, env) == "JB is great (JB)!");
}

string getVersionStrGit()
{
	import std.string : strip;

	auto result = tryRunCollect("git rev-parse --short HEAD");
	if(!result.status)
		return result.output.strip();

	return null;
}

string[string] env;

static this() {
    env = [
        "prefix" : "/usr/local",
        "exec_prefix" : "${prefix}",
        "bindir" : "${exec_prefix}/bin",
        "datarootdir" : "${prefix}/share",
        "datadir" : "${datarootdir}",
        "libdir" : "${exec_prefix}/lib",
        "localedir" : "${datarootdir}/locale"
    ];
}

int main(string[] args)
{
	auto doc = "DUB Prebuild useful command.

Usage:
  prebuild [-i input_file [-o output_file]] <PACKAGE_NAME> [-- <args>...]
  prebuild -h | --help
  prebuild --version

Options:
  -i input_file  Expand variables in this template
  -o output_file Write expanded version to the given output file
  -h --help      Show this screen.
  --version      Show version.
";
	auto arguments = docopt.docopt(doc, args[1..$], true, "DUB Prebuild v0.1");
    writeln("Current dir: " ~ getcwd());
    if (arguments["--"].isTrue) {
        writeln("Collecting arguments...");
        foreach(opt; arguments["<args>"].asList) {
            if(opt.startsWith("--")) {
                auto varMaybe = opt[2..opt.length].split("=");
                if (varMaybe.length == 2) {
                    env[varMaybe[0]] = varMaybe[1];
                }
            }
        }
    }
	if (!arguments["-i"].isNull) {
		auto bld_cfg_in = File(arguments["-i"].toString, "r");
		File output_file;
		scope(exit) {
			output_file.close();
			bld_cfg_in.close();
		}
		if (arguments["-o"]) {
			if (arguments["-o"].toString == "-") {
				output_file = stdout;
			} else {
				output_file = File(arguments["-o"].toString, "w");
			}
		} else {
			output_file = File(stripExtension(arguments["-i"].toString), "w");
		}
		env["PACKAGE_NAME"] = arguments["<PACKAGE_NAME>"].toString;
		env["GIT_VERSION"] = getVersionStrGit();
		foreach(l; bld_cfg_in.byLine().map!(l => expand(to!string(l), env))) {
			output_file.writeln(l);	
		}
	}
	
	// foreach(string name; dirEntries(".", "*.d", SpanMode.depth)) {
	// 	writeln(name);
	// }
	
	return 0;
}
