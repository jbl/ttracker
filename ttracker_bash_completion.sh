
_TTracker()
{
    local cur
    cur="${COMP_WORDS[COMP_CWORD]}"

    if [ $COMP_CWORD -eq 1 ]; then
        COMPREPLY=( $( compgen -W '--daemon -h --help -h --help --version add' -- $cur) )
    else
        case ${COMP_WORDS[1]} in
            add)
            _TTracker_add
        ;;
        esac

    fi
}

_TTracker_add()
{
    local cur
    cur="${COMP_WORDS[COMP_CWORD]}"

    if [ $COMP_CWORD -ge 2 ]; then
        COMPREPLY=( $( compgen -fW '-s= --start= -s= --start= -d= --description= -d= --description= -e= --end= -e= --end= ' -- $cur) )
    fi
}

complete -F _TTracker TTracker