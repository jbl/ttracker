TTracker - A small time tracking application
============================================

TTracker is a small Desktop (Linux, Windows, maybe others) application that helps you keep track of your activities.
It is greatly inspired by the fantastic Hamster time tracking "applet" but was born out of a few desires of mine:

* learn the D language
* use the same app on Windows and Linux
* implement the features that I wanted

