CREATE TABLE if not exists activity (
    id INTEGER PRIMARY KEY,
    uuid VARCHAR(255),
    low_name VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255),
    start INTEGER,
    end INTEGER,
    synced INTEGER,
    changed INTEGER,
    remote_id INTEGER
);
CREATE TABLE if not exists user (id INTEGER PRIMARY KEY, username VARCHAR(255) NOT NULL, password_hash VARCHAR(255) NOT NULL);
