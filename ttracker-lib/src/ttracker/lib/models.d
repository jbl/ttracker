// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.lib.models;

import core.stdc.time;
import std.datetime;
import std.json;

/**
   Activity, the main TTracker data structure.
*/
public struct Activity {
  /// An automatically filled locally unique identifier, typically a database ID
  long id;
  /// An automatically generated UUID, hopefully globally unique within the TTracker domain
  string uuid;
  /// The activity name, converted to lowercase. Users should not need to care about this one
  string low_name;
  /// The activity name
  string name;
  /// The activity description
  string description;
  /// The activity start date/time, in seconds from the Unix epoch
  time_t start;
  /// The activity end date/time, in seconds from the Unix epoch
  time_t end;
  /// Flag indicating whether this activity has been sync'ed
  int synced;
  int changed = 1;
  long remote_id;
}

JSONValue asJSON(ref Activity a)    {
    JSONValue result;
    result["id"] = JSONValue(a.id);
    result["uuid"] = JSONValue(a.uuid);
    result["low_name"] = JSONValue(a.low_name);
    result["name"] = JSONValue(a.name);
    result["description"] = JSONValue(a.description);
    result["start"] = JSONValue(SysTime(unixTimeToStdTime(a.start)).toISOExtString());
    result["end"] = JSONValue(SysTime(unixTimeToStdTime(a.end)).toISOExtString());
    return result;
}

/**
   User.
*/

public struct User {
  /// An automatically filled locally unique identifier, typically a database ID
  long id;
  string username;
  string password_hash;
}
