// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

/**
   Database Abstraction module to handle persistence and querying of Activity objects.

   This module provides CRUD functionality for Activity objects using an SQLite database by providing
   a convenience database abstraction object (TTrackerDb).
*/
module ttracker.lib.db;

import ddbc.core;
import ddbc.common;

import ttracker.lib.models;

interface IActivityRange {
    ref Activity front();
    void popFront();
    @property bool empty();
    IActivityRange filter(string filter);
    IActivityRange orderBy(string order);
    IActivityRange distinct();
    IActivityRange groupBy(string group);
}

interface ITTrackerDb {
  void addActivity(ref Activity a);
  bool createDb();
  void upgradeDb();
  void updateActivity(Activity a);
  void deleteActivity(Activity a);
  Activity getActivity(long id);
  Activity getActivityByUuid(string uuid);
  IActivityRange getActivities();
  Connection getConnection();
}
