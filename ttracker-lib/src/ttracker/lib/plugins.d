// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

/**
 * TTracker plugins - allow extending the functionality through 3rd party extensions.
 */
module ttracker.lib.plugins;

public import std.json;
public import std.datetime;
public import gtk.Window;

public import ttracker.lib;

/**
 * This is the generic interface that all plugins must implement to be recognized.
 * Plugins are organised in categories which group plugins by the type of functionality they provide.
 */
interface IPlugin {

  /**
   * Get the name of the plugin, as shown in, for example, the "Preferences" dialog
   * Returns: string containing the plugin name, shown in the plugin manager UI
   */
  immutable(string) get_name();

  /**
   * Called by the "Preferences" dialog so plugins can show an "About" box
   * Params:
   *    parent = Handle to TTracker's Preference dialog, to parent the about box properly
   */
  void about(Window parent);

  /**
   * Called by the "Preferences" dialog to decide whether to enable the plugin specific "Preferences" button
   * Returns: true if the plugin implements a "preferences" dialog
   */
  bool has_preferences();
  /**
   * Called by the "Preferences" dialog so plugins can show their own "Preferences" dialog
   * Params:
   *    parent = Handle to TTracker's Preference dialog, to parent the preferences dialog properly
   *    prefs = instance of a IPluginPreferences implementation that plugins can use read/store their preferences
   */
  void preferences(Window parent, IPluginPreferences prefs);
}

/**
 * Interface for synchronisation plugins.
 */
interface ISyncPlugin : IPlugin {
    /**
     * Called by TTracker at regular intervals (not so good, I know).
     * Plugins should create a background thread or similar so as to not block the UI during sync.
     * Params:
     *    db = instance of TTracker's Activity database
     *    prefs = instance of a IPluginPreferences implementation that plugins can use read/store their preferences
     */
    void sync(ITTrackerDb db, IPluginPreferences prefs);
}

/**
 * Interface for reporting plugins.
 */
interface IReportPlugin : IPlugin {
    /**
     * Called by TTracker when user chooses this plugins when creating a report.
     * Params:
     *    db = instance of TTracker's Activity database
     *    output = a string identifying where the report is to be generated. Most likely a filename
     *    start = the start date filter as set by the user through the UI
     *    end = the end date filter
     */
    void report(ref ITTrackerDb db, string output, Date start, Date end);
}

/**
 * Plugins preferences interface: allow plugin to store/retrieve their preferences
 */
public interface IPluginPreferences {
    @property {
        /**
        * Set the preferences for a plugin
        *
        * Params:
        *      plugins_prefs = the plugin's preferences to be set
        */
        public void plugin_preferences(JSONValue plugin_prefs);
        /**
        * Retrieve the preferences for a plugin
        *
        * Return:a JSONValue with the plugin's current preferences
        */
        public JSONValue plugin_preferences();
    }
}

/**
 * Plugins Descriptor interface.
 * This interface is the handshake between plugins and TTracker's core.
 * Plugins have properties (a category, a name, maybe a version at later stage) and an implementation.
 * This interface bundles the 2...
 */
interface IPluginDescriptor {

    /**
     * Plugin description, basically a piece of const data with bits of information about a plugin.
     */
    struct PluginDescription {
        string name; /// The plugin name, duh!
        string category; /// The plugin category, will probably be refactored
    }

    /**
     * Obtain the descriptor of the plugin, this should be callable without an actual plugin instance!
     * Returns: a PluginDescription containing this plugins's information
     */
    PluginDescription get_description() const;

    /**
     * Obtain the plugin implementation, conforming at least to the IPlugin interface
     * Returns: the plugin implementation, derived from IPlugin
     */
    IPlugin get_instance();
}
