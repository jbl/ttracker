// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.lib.utils;

public import gdk.Pixbuf : Pixbuf;
import gdkpixbuf.PixbufLoader : PixbufLoader;
import glib.Bytes : Bytes;
import std.datetime : SysTime, DateTime, Clock, dur, Date;
import std.file;
import std.path;
import std.process;

/**
 * Access the translated version if available of the given string.
 *
 * Use this function to retrieve any user-facing string (i.e. strings that are shown to the user).
 * Using this function to wrap all user-facing strings makes them automatically extractable by the GNU dgettext
 * tool suite.
 *
 * Params:
 *      text: the string to lookup, which is returned as is if no translation for it is found
 * Return: the translated string if available or the given string
 *
 */
string _(string text)
{
    import glib.Internationalization : Internationalization;

    return Internationalization.dgettext("TTracker", text);
}

/**
 * Create and return a Pixbuf object from memory data. Useful if you want to embed icons or graphics in the executable.
 *
 * Params:
 *     data = immutable ubyte[] array containing PixbufLoader compatible image data (PNG, JPEG), obtained for example using "import".
 *
 * Return: a Pixbuf object or null if something went wrong.
 *
 */
static public Pixbuf get_pixbuf_from_data(immutable ubyte[] data)
{
    return get_pixbuf_from_data(cast(ubyte[]) data);
}

/**
 * Create and return a Pixbuf object from memory data. Useful if you want to embed icons or graphics in the executable.
 *
 * Params:
 *     data = ubyte[] array containing PixbufLoader compatible image data (PNG, JPEG), obtained for example using "import".
 *
 * Return: a Pixbuf object or null if something went wrong.
 *
 */
static public Pixbuf get_pixbuf_from_data(ubyte[] data)
{
    PixbufLoader loader = new PixbufLoader();
    loader.writeBytes(new Bytes(data));
    loader.close();
    return loader.getPixbuf();
}

/**
 * Return a SysTime object set to <current_day> 00h 00m 00s
 *
 * Return: SysTime
 */
static public SysTime get_midnight_today()
{
    auto today = Clock.currTime();
    today.hour = 0;
    today.minute = 0;
    today.second = 0;
    return SysTime(DateTime(today.year, today.month, today.day));
}

/**
 * Return a SysTime object set to 00h 00m 00s of the given day
 *
 * Return: SysTime
 */
static public SysTime get_midnight(SysTime day)
{
    return SysTime(DateTime(day.year, day.month, day.day));
}

/**
 * Return a SysTime object set to 23h 59m 59s of the given day
 *
 * Return: SysTime
 */
static public SysTime get_before_day_after(SysTime day)
{
    auto res = SysTime(DateTime(day.year, day.month, day.day));
    res.hour = 23;
    res.minute = 59;
    res.second = 59;
    return res;
}

/**
 * Return a SysTime object set to the day before the given day
 *
 * Return: SysTime
 */
static public SysTime get_day_before(SysTime day)
{
    auto res = day - dur!"days"(1);
    return res;
}

/**
 * Return a SysTime object set to the day after the given day
 *
 * Return: SysTime
 */
static public SysTime get_day_after(SysTime day)
{
    auto res = day + dur!"days"(1);
    return res;
}

/**
 * Return a SysTime object set to <current_day> 00h 00m 00s
 *
 * Return: SysTime
 */
static public SysTime get_first_day_of_month()
{
    auto today = Clock.currTime();
    today.hour = 0;
    today.minute = 0;
    today.second = 0;
    return SysTime(DateTime(today.year, 1, today.day));
}

/**
 * Return the platform independent user config directory (and ensure it exists)
 *
 * Return: string
 */
static string get_user_config_dir()
{
    string result;
    version (Windows)
    {
        result = environment["APPDATA"];
    }
    version (Posix)
    {
        auto home = environment.get("HOME", "");
        auto config = buildPath(home, ".config");
        result = environment.get("XDG_CONFIG_HOME", config);
        if (!exists(result))
        {
            mkdir(result);
        }
    }
    return result;
}

/**
 * Return a subdirectory under the platform independent user config directory (and ensure it exists)
 *
 * Return: string
 */
static string get_user_config_subdir(string subdir = "")
{
    string result = get_user_config_dir();
    result = buildPath(result, subdir);
    if (!exists(result))
    {
        mkdir(result);
    }
    return result;
}

/// TODO: document and test this
struct DateRange {
    private Date start;
    private Date end;
    private Date current;

    this(Date start, Date end) {
        this.start = start;
        this.end = end;
        this.current = start;
    }

    @property bool empty() const {
        return this.current > this.end;
    }
    @property ref Date front() {
        return this.current;
    }
    void popFront() {
        this.current += dur!"days"(1);
    }
}

import std.experimental.logger;
import std.conv;
import std.stdio;
import std.concurrency : Tid;
import std.format : formattedWrite;

class FileLoggerWithLevel : FileLogger {
    this(in string fn, const LogLevel lv = LogLevel.all) @safe
    {
        super(fn, lv);
    }

    this(File file, const LogLevel lv = LogLevel.all) @safe
    {
        super(file, lv);
    }
    /// ditto
    override protected void beginLogMsg(string file, int line, string funcName,
        string prettyFuncName, string moduleName, LogLevel logLevel,
        Tid threadId, SysTime timestamp, Logger logger)
        @safe
    {
        import std.string : lastIndexOf;
        ptrdiff_t fnIdx = file.lastIndexOf('/') + 1;
        ptrdiff_t funIdx = funcName.lastIndexOf('.') + 1;

        auto lt = this.file.lockingTextWriter();
        systimeToISOString(lt, timestamp);
        formattedWrite(lt, ":%s:%s:%s:%u ", to!string(logLevel), file[fnIdx .. $],
            funcName[funIdx .. $], line);
    }

}
