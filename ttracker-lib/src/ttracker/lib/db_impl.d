// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

/**
   Database Abstraction module to handle persistence and querying of Activity objects.

   This module provides CRUD functionality for Activity objects using an SQLite database by providing
   a convenience database abstraction object (TTrackerDb).
*/
module ttracker.lib.db_impl;

import ddbc.core;
import ddbc.common;
import ddbc.drivers.sqliteddbc;
import ddbc.pods;

import std.algorithm;
import std.array;
import std.conv;
import std.format;
import std.stdio;
import std.uuid;

import ttracker.lib.models;
import ttracker.lib.db;

class TTrackerDb : ITTrackerDb {
  ConnectionPoolDataSourceImpl ds;
  public this(ConnectionPoolDataSourceImpl ds) {
    // prepare database connectivity
    this.ds = ds;
    auto conn = ds.getConnection();
    scope(exit) conn.close();
    Statement stmt = conn.createStatement();
    scope(exit) stmt.close();
    foreach(sql; split(import("ttracker.sql"), ";").filter!(a => a != "\n")) {
      stmt.executeUpdate(sql);
    }
  }

  bool createDb() {
    return false;
  }

  void upgradeDb() {
  }

  private static string db_statement(string op) {
    return `
    {
      auto conn = ds.getConnection();
      scope(exit) conn.close();
      Statement stmt = conn.createStatement();
      scope(exit) stmt.close();
      ` ~ op ~ `
    }`;
  }

  Connection getConnection() {
    return ds.getConnection();
  }

  void addActivity(ref Activity a) {
    if (a.uuid.length == 0) {
      auto id = randomUUID();
      a.uuid = id.toString();
    }
    mixin(db_statement("stmt.insert(a);"));
  }

  void updateActivity(Activity a) {
    if (a.uuid.length == 0) {
      auto id = randomUUID();
      a.uuid = id.toString();
    }
    mixin(db_statement("stmt.update(a);"));
  }

  void deleteActivity(Activity a) {
    mixin(db_statement("ddbc.pods.remove(stmt,a);"));
  }

  Activity getActivity(long id) {
    Activity result;
    try {
        mixin(db_statement("result = stmt.get!Activity(id);"));
    } catch (SQLException e) {
      throw new Exception("Activity not found");
    }
    return result;
  }

  Activity getActivityByUuid(string uuid) {
    Activity result;
    try {
      mixin(db_statement("result = stmt.get!Activity(\"uuid=uuid\");"));
    } catch (SQLException e) {
      throw new Exception("Activity not found");
    }
    return result;
  }

  class ActivityRange : IActivityRange {
    private TTrackerDb db;
    private Connection conn;
    private Statement stmt;
    private select!Activity select_range;

    this(TTrackerDb db) {
      this.db = db;
      this.conn = db.getConnection();
      this.stmt = conn.createStatement();
      this.select_range = select!Activity(stmt);
    }

    ~this() {
    }
    ref Activity front() {
      return this.select_range.front();
    }
    void popFront() {
      this.select_range.popFront();
    }
    @property bool empty() {
      auto res = this.select_range.empty();
      if(res) {
        this.stmt.close();
        this.conn.close();
      }
      return res;
    }
    IActivityRange filter(string filter) {
      this.select_range.where(filter);
      return this;
    }
    IActivityRange orderBy(string order) {
      this.select_range.orderBy(order);
      return this;
    }
    IActivityRange distinct() {
      this.select_range.distinct();
      return this;
    }
    IActivityRange groupBy(string group) {
      this.select_range.groupBy(group);
      return this;
    }
  }

  IActivityRange getActivities() {
    return new ActivityRange(this);
  }

  void addUser(User u) {
    mixin(db_statement("stmt.insert(u);"));
  }

  User[] getUsers(string username) {
    User[] result;
    mixin(db_statement("foreach(ref u;stmt.select!User().where(\"username='\"~username~\"'\")) { result ~= u; }"));
    return result;
  }
}
