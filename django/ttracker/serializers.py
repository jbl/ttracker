# The MIT License (MIT)
#
# Copyright (c) 2015 Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from rest_framework import serializers
from django.contrib.auth import get_user_model
# from django.db.models import Q, Sum
from ttracker.models import Activity, Project, Timesheet

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    total = serializers.ReadOnlyField()
    worked = serializers.ReadOnlyField()
    sickness = serializers.ReadOnlyField()
    holidays = serializers.ReadOnlyField()

    # def get_total(self, obj):
    #     return obj.timesheet_set.all().aggregate(total=Sum('activity__duration'))['total']

    # def get_worked(self, obj):
    #     sickness = Q(project__name__icontains="sick")
    #     holidays = Q(project__name__icontains="holiday")
    #     return Activity.objects.filter(timesheet__user=obj).exclude(sickness).exclude(holidays).aggregate(total=Sum('duration'))['total']

    # def get_sickness(self, obj):
    #     sickness = Q(project__name__icontains="sick")
    #     return Activity.objects.filter(timesheet__user=obj).filter(sickness).aggregate(total=Sum('duration'))['total']

    # def get_holidays(self, obj):
    #     holidays = Q(project__name__icontains="holidays")
    #     return Activity.objects.filter(timesheet__user=obj).filter(holidays).aggregate(total=Sum('duration'))['total']

    class Meta:
        model = get_user_model()
        fields = ('username', 'total', 'worked', 'sickness', 'holidays')

class ActivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ('pk', 'uuid', 'start', 'end', 'name', 'description')

class TimesheetSerializer(serializers.ModelSerializer):
    activities = ActivitySerializer(source='activity_set', many=True, required=False)
    class Meta:
        model = Timesheet
        fields = ('id', 'month', 'activities')
