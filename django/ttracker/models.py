# The MIT License (MIT)
#
# Copyright (c) 2017 Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth import get_user_model
from django.conf import settings

class Project(models.Model):
    """ A project is an entity where Activities can be registered with
    """
    TYPES = (
        (0, "Normal work"),
        (1, "Sickness"),
        (2, "Child Sickness"),
        (3, "Holidays"),
        (4, "National Holidays"),
    )
    name = models.CharField(max_length=255, help_text=_("A meaning full name for the project"))
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="owner")
    participants = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="participants", blank=True)
    type = models.IntegerField(choices=TYPES, default=0)
    def __unicode__(self):
        return "{name} - {owner!s}".format(**dict(
            name=self.name,
            owner=self.owner
        ))

class Timesheet(models.Model):
    month = models.DateField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True,)
    locked = models.BooleanField(default=False)

    class Meta:
        unique_together = ('month', 'user',)

    def __unicode__(self):
        return "{month!s} - {user!s}".format(**dict(
            month=self.month,
            user=self.user
        ))

def gen_uuid():
    import uuid
    return uuid.uuid4()

class Activity(models.Model):
    """ An activity is the unit of time tracking
    """
    name = models.CharField(max_length=255, blank=True, null=True)
    uuid = models.UUIDField(db_index=True, unique=True, default=gen_uuid)
    description = models.CharField(max_length=255, blank=True, null=True)
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    # This field is there only to work around an SQLite limitation that does not allow to select
    # time difference
    duration = models.FloatField(null=True, blank=True)
    class Meta:
        db_table = 'activity'

    def __unicode__(self):
        return "{name!s} - {start!s}".format(**dict(
            name=self.name,
            start=self.start
        ))

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.end and self.start:
            self.duration = (self.end - self.start).total_seconds() / 60.0
        super(Activity, self).save(force_insert, force_update, using, update_fields)
