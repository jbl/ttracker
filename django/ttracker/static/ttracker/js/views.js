var router;
(function ($) {
    'use strict';
    var ActivityModel = Backbone.Model.extend({
        url: '/api/activities/'
    });
    var UserModel = Backbone.Model.extend({
        url: '/api/users/'
    });
    var UserCollection = Backbone.Collection.extend({
        model: UserModel,
        url: '/api/users/'
    });
    var CurrentTimeSheetModel = Backbone.Model.extend({
        url: '/api/timesheets/current/'
    });

    var ProjectRow = Backbone.View.extend({
        "template": _.template($("#project-row-template").html()),
        "tagName": "tr",
        "events": {
            "focus .duration": "on_focus",
            "blur .duration": "on_blur",
            "change .duration": "on_change",
            "keyup .duration": "on_keyup",
            "change select": "on_project_change"
        },
        initialize: function() {
            this.activities = {};
            this.project = -1;
            this.render();
        },
        render: function () {
            this.$el.html(this.template({})); //this.model.attributes));
            this.$('select', this.$el).button();
        },
        set_project: function(project, name) {
            this.project = project;
            this.$("#project-name").html(name);
        },
        on_project_change: function() {
            this.project = parseInt(this.$('select', this.$el).val());
        },
        on_change: function(e) {
            var value = parseFloat($(e.target).val());
            $(e.target).attr("value-changed", true);
            $(e.target).attr("previous-value", value);
            var new_value = value;
            var day = parseInt(e.target.id);
            // the month parameter of the Date constructor is 0-based
            var start = new Date(year, month_number - 1, day, 1, 0, 0);
            var end = new Date(start.getTime() + new_value * 60 * 60 * 1000);
            var activity = this.activities[day] || {};
            activity.start = start;
            activity.end = end;
            this.activities[day] = activity;
            this.trigger("hoursChanged", e.target.id);
        },
        on_focus: function(e) {
            var value = parseFloat($(e.target).val());
            if (value == 0) {
                $(e.target).val(7.4);
                $(e.target).attr("value-changed", false);
            } else {
                $(e.target).attr("previous-value",$(e.target).val());
            }
        },
        on_blur: function(e) {
            var changed = $(e.target).attr("value-changed") && $(e.target).attr("value-changed") == 'true';
            var day = parseInt(e.target.id);
            if (changed) {
                var new_value = parseFloat($(e.target).val());
                // the month parameter of the Date constructor is 0-based
                var start = new Date(year, month_number - 1, day, 1, 0, 0);
                var end = new Date(start.getTime() + new_value * 60 * 60 * 1000);
                var activity = this.activities[day] || {};
                activity.start = start;
                activity.end = end;
                this.activities[day] = activity;
            } else {
                var previous = $(e.target).attr("previous-value");
                if (previous) {
                    $(e.target).val(previous);
                } else {
                    $(e.target).val(0);
                    delete this.activities[day];
                }
            }
        },
        on_keyup: function(e) {
            if(e.keyCode == 13) {
                $(e.target).attr("value-changed", true);
                var day = parseInt(e.target.id);
                var new_value = parseFloat($(e.target).val());
                // the month parameter of the Date constructor is 0-based
                var start = new Date(year, month_number - 1, day, 1, 0, 0);
                var end = new Date(start.getTime() + new_value * 60 * 60 * 1000);
                var activity = this.activities[day] || {};
                activity.start = start;
                activity.end = end;
                this.activities[day] = activity;
                this.trigger("hoursChanged", e.target.id);
            }
        },
        add_activity: function(activity) {
            var start_date = new Date(activity.start);
            var end_date = new Date(activity.end);
            var index = start_date.getDate() - 1;
            var current_value = parseFloat($(this.$('input')[index]).val());
            var duration = (end_date.getTime() - start_date.getTime()) / 1000 / 60 / 60;
            this.activities[start_date.getDate()] = activity;
            $(this.$('input')[index]).val(current_value + duration);
        }
    });

    var CurrentTimeSheetView = Backbone.View.extend({
        "template": _.template($("#current-timesheet-template").html()),
        "el": $("#timesheets"),
        "events": {
            "click #new-project": "on_new_project_click",
            "click #validate": "on_validate_click",
            "change #selected-user": "on_user_changed"
        },
        initialize: function() {
            this.listenTo(this.model, "sync", this.render);
            this.render();
        },
        on_user_changed: function() {
            current_user = this.user_combo.val();
            $("#user-info").html(this.$("#" + this.user_combo[0].id + " option:selected").text());
            this.model.fetch({
                data: {
                    user: current_user
                }
            });
        },
        render: function() {
            var data = _.clone(this.model.attributes);
            var today = new Date();
            data.month = (today.getMonth() + 1).toString() + "/" + today.getFullYear().toString();
            this.$el.html(this.template(data));
            this.user_combo = this.$("#selected-user")
            this.$("button").button();
            this.$("select").button();
            this.hours_total = this.$("#hours-total");
            var self = this;
            var projects = {};
            var hours_total = 0.0;
            this.rows = [];
            _.each(this.model.get('activities'), function(one_activity, index) {
                var row;
                if (!projects[one_activity.project]) {
                    row = new ProjectRow();
                    self.listenTo(row, "hoursChanged", self.on_hours_changed);
                    self.rows.push(row);
                    projects[one_activity.project] = row;
                    row.set_project(one_activity.project, self.$("#project option[value=" + one_activity.project + "]").text());
                    self.$("#project option[value=" + one_activity.project + "]").remove();
                    if(self.$("#project option").length == 0){
                        self.$("#project-area").hide();
                    }
                } else {
                    row = projects[one_activity.project];
                }
                row.add_activity(one_activity);
                hours_total += (new Date(one_activity.end).getTime() - new Date(one_activity.start).getTime()) / 1000.0 / 60.0 / 60.0
                self.$("#timesheet-table", self.$el).append(row.$el);
            });
            _.each(this.$("tfoot>tr>td", this.$el), function(td, index) {
                if (index > 0) {
                    var col_idx = index;
                    var total = _.reduce(self.$(".column-" + col_idx.toString()), function(memoizer, curr) {
                        return memoizer + parseFloat($(curr).val());
                    }, 0.0);
                    $(td).html(round(total, 2));
                }
            });
            if(this.model.get('activities').length == 0) {
                this.on_new_project_click();
            }
            this.user_combo.val(current_user);
            this.hours_total.html(round(hours_total, 2));
            return this;
        },
        on_hours_changed: function(day) {
            var hours_total = 0.0
            _.each(this.rows, function(row, index) {
                _.each(row.activities, function(a) {
                    var d = new Date(a.end).getTime() - new Date(a.start).getTime();
                    hours_total += d;
                });
            });
            this.hours_total.html(hours_total / 1000.0 / 60.0 / 60.0);
            var total = _.reduce(this.$(".column-" + day.toString()), function(memoizer, curr) {
                return memoizer + parseFloat($(curr).val());
            }, 0.0);
            this.$(".total-" + day.toString()).html(round(total, 2));
        },
        on_new_project_click: function(e) {
            var row = new ProjectRow();
            var selected_project = parseInt(this.$("#project").val());
            var selected_project_name = this.$("#project option:selected").text();
            this.$("#project option:selected").remove();
            if(this.$("#project option").length == 0){
                this.$("#project-area").hide();
            }
            this.rows.push(row);
            row.set_project(selected_project, selected_project_name);
            this.listenTo(row, "hoursChanged", this.on_hours_changed);
            this.$("#timesheet-table", this.$el).append(row.$el);
        },
        on_validate_click: function(e) {
            this.model.set("activities", []);
            var self = this;
            _.each(this.rows, function(row, index) {
                var project = row.project;
                _.each(row.activities, function(activity, index) {
                    var activity_model = new ActivityModel(activity);
                    activity_model.set('project', project);
                    activity_model.set('timesheet', self.model.get('id'));
                    self.model.get("activities").push(activity_model);
                });
            });
            this.model.set("user", current_user)
            this.model.save();
        }
    });

    function round(value, decimals) {
        return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
    }
    var viewHelpers = {
        round: round
    };

    var UserRowView = Backbone.View.extend({
        "template": _.template($("#user-row-template").html()),
        "tagName": "tr",
        "events": {
        },
        initialize: function() {
            this.render();
        },
        render: function() {
            var config = _.extend(this.model.attributes, viewHelpers);
            this.$el.append(this.template(config));
            return this;
        }
    });

    var AdminView = Backbone.View.extend({
        "template": _.template($("#admin-template").html()),
        "el": $("#admin"),
        "events": {
        },
        initialize: function() {
            this.render();
        },
        render: function() {
            var self = this;
            var data = _.clone(this.collection.attributes);
            this.$el.html(this.template(data));
            this.tbody = this.$('tbody').html('');
            _.each(this.collection.models, function(model, index) {
                var row = new UserRowView({
                    model: model
                });
                self.tbody.append(row.el);
            });
        }
    });

    var Router = Backbone.Router.extend({
        routes: {
            "current": "showCurrent",
            "admin": "showAdmin"
        },
        setView: function(view) {
            if(this.view) {
                this.view.remove();
            }
            this.view = view;
            $("#content").append($(this.view.el));
        },
        showCurrent: function() {
            var timesheet = new CurrentTimeSheetModel();
            timesheet.fetch().then(function () {
                var view = new CurrentTimeSheetView({
                    model: timesheet
                });
            });
        },
        showAdmin: function() {
            var userlist = new UserCollection();
            userlist.fetch().then(function() {
                var view = new AdminView({
                    collection: userlist
                });
            });
        }
    });

    router = new Router();
    Backbone.history.start({pushState: true});
    router.navigate("current", true);
})(jQuery);
