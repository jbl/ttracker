from django.conf import settings

# A callable
def ALLAUTH_O365_COMPLETE_LOGIN_HOOK(app, token):
    import requests
    organization_url = 'https://graph.microsoft.com/v1.0/organization'
    headers = {'Authorization': 'Bearer {0}'.format(token.token)}
    resp = requests.get(organization_url, headers=headers)
    extra_data = resp.json()
    if not extra_data['value'][0]['displayName'] == 'Xtel ApS':
        raise Exception("Only accounts from Xtel ApS are allowed")


def get_settings(setting):
    return getattr(settings, setting, globals().get(setting))
