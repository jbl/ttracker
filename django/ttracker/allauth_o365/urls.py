from allauth.socialaccount.providers.oauth.urls import default_urlpatterns

from .provider import Office365Provider

urlpatterns = default_urlpatterns(Office365Provider)
