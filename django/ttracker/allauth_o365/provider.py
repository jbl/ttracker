from __future__ import unicode_literals

from allauth.socialaccount import providers
from allauth.socialaccount.providers.base import ProviderAccount
from allauth.socialaccount.providers.oauth2.provider import OAuth2Provider


class Office365Account(ProviderAccount):

    def to_str(self):
        name = self.account.extra_data.get('displayName')
        if name.strip() != '':
            return name
        return super(Office365Account, self).to_str()


class Office365Provider(OAuth2Provider):
    id = str('office365')
    name = 'Office 365'
    account_class = Office365Account

    def get_default_scope(self):
        return ['User.Read']

    def extract_uid(self, data):
        return str(data['id'])

    def extract_common_fields(self, data):
        try:
            email = data.get('mail')
        except:
            email = data.get('userPrincipalName')
        
        return dict(
            username=data.get('userPrincipalName'),
            email=email,
            last_name=data.get('surname'),
            first_name=data.get('givenName')
        )


providers.registry.register(Office365Provider)
