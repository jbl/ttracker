# The MIT License (MIT)
#
# Copyright (c) 2015 Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import time
import datetime
import json

from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.db.models import Q, Sum

from ttracker.models import Activity, Project, Timesheet
from ttracker.serializers import ActivitySerializer, ProjectSerializer, TimesheetSerializer, UserSerializer

import dateutil.parser
from dateutil.relativedelta import relativedelta


class ProjectViewSet(viewsets.ModelViewSet):
    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer

    def get_total(self, obj, date_range):
        start = Q(start__gte=date_range[0])
        end = Q(end__lt=date_range[1])
        return Activity.objects.filter(start, end, timesheet__user=obj).aggregate(total=Sum('duration'))['total']

    def get_worked(self, obj, date_range):
        sickness = Q(project__name__icontains="sick")
        holidays = Q(project__name__icontains="holiday")
        start = Q(start__gte=date_range[0])
        end = Q(end__lt=date_range[1])
        return Activity.objects.filter(start, end, timesheet__user=obj).exclude(sickness).exclude(holidays).aggregate(total=Sum('duration'))['total']

    def get_sickness(self, obj, date_range):
        sickness = Q(project__name__icontains="sick")
        start = Q(start__gte=date_range[0])
        end = Q(end__lt=date_range[1])
        return Activity.objects.filter(start, end, timesheet__user=obj).filter(sickness).aggregate(total=Sum('duration'))['total']

    def get_holidays(self, obj, date_range):
        holidays = Q(project__name__icontains="holidays")
        start = Q(start__gte=date_range[0])
        end = Q(end__lt=date_range[1])
        return Activity.objects.filter(start, end, timesheet__user=obj).filter(holidays).aggregate(total=Sum('duration'))['total']

    def get_queryset(self):
        now = datetime.date.today()
        start = datetime.date(now.year, now.month, 1)
        if self.request.GET.get('month', None):
            start = dateutil.parser.parse(self.request.GET('month'))
        else:
            start = start
        end = start + relativedelta(months=1)
        date_range = [start, end]
        return [
            {
                'total': self.get_total(x, date_range),
                'worked': self.get_worked(x, date_range),
                'sickness': self.get_sickness(x, date_range),
                'holidays': self.get_holidays(x, date_range),
                'username': x.username
            } for x in get_user_model().objects.all()
        ]


from rest_framework.authentication import TokenAuthentication
class ActivityViewSet(viewsets.ModelViewSet):

    class CsrfExemptSessionAuthentication(SessionAuthentication):

        def enforce_csrf(self, request):
            # To not perform the csrf check previously happening
            return

    authentication_classes = (TokenAuthentication, CsrfExemptSessionAuthentication,)
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer

    def get_serializer(self, *args, **kwargs):
        if isinstance(kwargs.get("data"), list):
            kwargs["many"] = True
        return super(ActivityViewSet, self).get_serializer(*args, **kwargs)

    def perform_create(self, serializer):
        data = serializer.validated_data
        uuid = data['uuid']
        data['user_id'] = self.request.user.id
        activity, _ = Activity.objects.update_or_create(uuid=uuid, defaults=data)
        data['pk'] = activity.id


class TimesheetViewSet(viewsets.GenericViewSet):
    class CsrfExemptSessionAuthentication(SessionAuthentication):

        def enforce_csrf(self, request):
            # To not perform the csrf check previously happening
            return

    authentication_classes = (CsrfExemptSessionAuthentication,)
    queryset = Activity.objects.all()
    serializer_class = TimesheetSerializer

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Activity.objects.all()
        else:
            return Activity.objects.filter(user=self.request.user)

    @list_route(methods=['get', 'put'])
    def current(self, request, *args, **kw):
        now = datetime.date.today()
        start = datetime.date(now.year, now.month, 1)
        if request.method == 'GET':
            user_id = request.GET.get('user', request.user.id)
        else:
            data = json.loads(request.body)
            user_id = data.get('user', request.user.id)
        timesheet, _ = Timesheet.objects.get_or_create(month=start, user_id=user_id, defaults=dict(month=start, user_id=user_id))
        if request.method == 'GET':
            serializer = TimesheetSerializer(timesheet)
            return Response(serializer.data)
        else:
            data['month'] = start
            activities = data['activities']
            existing_activities = [x for x in activities if x.has_key('id')]
            existing_activities_obj = Activity.objects.filter(id__in=[x["id"] for x in existing_activities])
            if existing_activities:
                for data, obj in zip(existing_activities, existing_activities_obj):
                    activity = ActivitySerializer(instance=obj, data=data)
                    activity.is_valid()
                    data.setdefault('project_id', data.pop('project', activity.instance.project_id))
                    del data['timesheet']
                    for attr, value in data.items():
                        setattr(activity.instance, attr, value)
                    if activity.instance.start != activity.instance.end:
                        if not isinstance(activity.instance.start, datetime.datetime):
                            activity.instance.start = dateutil.parser.parse(activity.instance.start)
                        if not isinstance(activity.instance.end, datetime.datetime):
                            activity.instance.end = dateutil.parser.parse(activity.instance.end)
                        activity.instance.save()
                    else:
                        activity.instance.delete()
            new_activities = [x for x in activities if not x.has_key('id')]
            if new_activities:
                activities = ActivitySerializer(data=new_activities, many=True)
                activities.is_valid()
                activities.save()
            timesheet.refresh_from_db()
            serializer = TimesheetSerializer(timesheet)
            return Response(serializer.data)


@login_required
def index(req):
    def enumerate_days():
        now = datetime.datetime.utcnow()
        start = datetime.datetime(now.year, now.month, 1, 0, 0, 0, 0)
        month = start.month
        current = datetime.datetime(now.year, now.month, 1, 0, 0, 0, 0)
        while current.month == month:
            yield current, int(time.mktime(current.timetuple())) * 1000
            current += datetime.timedelta(days=1)
    if req.user and req.user.is_superuser:
        projects = Project.objects.all()
        users = get_user_model().objects.all().values('id', 'username')
    else:
        projects = Project.objects.filter(
            Q(participants=req.user) | Q(participants__isnull=True))
        users = []
    now = datetime.datetime.utcnow()
    month = datetime.datetime(now.year, now.month, 1, 0, 0, 0, 0)

    return render(req, 'ttracker/index.html', {
        'enumerate_days': enumerate_days,
        'projects': projects,
        'month': month,
        'users': users
    })
