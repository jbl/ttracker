// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.client.preferences;

public import std.file;
public import std.format;
public import std.json;
public import std.path;
public import std.process;
public import std.stdio;

public import ttracker.lib;
import ttracker.lib.plugins;

/**
 */
class Preferences
{
    enum desktop_file_data = import("ttracker.desktop");
    string config_dir;
    bool initialized;
    JSONValue preferences;
    string pref_file;
    string desktop_file;
    JSONValue[string] plugins_preferences;

    @property public void plugin_preferences(string plugin_name, JSONValue plugin_prefs)
    {
        ensureInit();
        plugins_preferences[plugin_name] = plugin_prefs;
        auto prefs_file = buildPath(config_dir, plugin_name ~ ".json");
        std.file.write(prefs_file, plugin_prefs.toString());
    }

    @property public JSONValue plugin_preferences(string plugin_name)
    {
        JSONValue result;
        ensureInit();
        if (plugin_name in plugins_preferences)
        {
            result = plugins_preferences[plugin_name];
        }
        else
        {
            auto prefs_file = buildPath(config_dir, plugin_name ~ ".json");
            if (!exists(prefs_file))
            {
                std.file.write(prefs_file, "{}");
            }
            result = parseJSON(cast(string) read(prefs_file));
            plugins_preferences[plugin_name] = result;
        }
        return result;
    }

    @property public void autostart(bool start)
    {
        ensureInit();
        if (start)
        {
            copyDesktopFile();
        }
        else
        {
            removeDesktopFile();
        }
    }

    @property public bool autostart()
    {
        ensureInit();
        return exists(desktop_file);
    }

    @property public void show_window_at_launch(bool show)
    {
        ensureInit();
        preferences["show_window_at_launch"] = JSONValue(show);
        std.file.write(pref_file, preferences.toString());
    }

    @property public bool show_window_at_launch()
    {
        ensureInit();
        return preferences["show_window_at_launch"].type == JSON_TYPE.TRUE;
    }

    @property public void close_active_task_on_exit(bool show)
    {
        ensureInit();
        preferences["close_active_task_on_exit"] = JSONValue(show);
        std.file.write(pref_file, preferences.toString());
    }

    @property public bool close_active_task_on_exit()
    {
        ensureInit();
        return preferences["close_active_task_on_exit"].type == JSON_TYPE.TRUE;
    }

    public bool plugin_enabled(string plugin_name)
    {
        if ("enabled_plugins" in preferences)
        {
            if (plugin_name in preferences["enabled_plugins"])
            {
                return preferences["enabled_plugins"][plugin_name].type == JSON_TYPE.TRUE;
            }
        }
        else
        {
            preferences["enabled_plugins"] = parseJSON("{}");
            std.file.write(pref_file, preferences.toString());
        }
        return false;
    }

    public void plugin_enabled(string plugin_name, bool enabled)
    {
        if ("enabled_plugins" !in preferences)
        {
            preferences["enabled_plugins"] = parseJSON("{}");
        }
        preferences["enabled_plugins"].object[plugin_name] = JSONValue(enabled);
        std.file.write(pref_file, preferences.toString());
    }

    private void copyDesktopFile()
    {
        auto expanded_data = format(desktop_file_data, thisExePath());
        std.file.write(desktop_file, expanded_data);
    }

    private void removeDesktopFile()
    {
        remove(desktop_file);
    }

    private void ensureInit()
    {
        if (!initialized)
        {
            config_dir = get_user_config_subdir("ttracker");
            pref_file = buildPath(config_dir, "ttracker.json");
            auto autostart_dir = buildPath(get_user_config_dir, "autostart");
            desktop_file = buildPath(autostart_dir, "ttracker.desktop");
            if (!exists(pref_file))
            {
                preferences["show_window_at_launch"] = true;
                preferences["close_active_task_on_exit"] = false;
                std.file.write(pref_file, preferences.toString());
            }
            preferences = parseJSON(cast(string) read(pref_file));
            if("close_active_task_on_exit" !in preferences) {
                preferences["close_active_task_on_exit"] = false;
                preferences["show_window_at_launch"] = true;
            }
            initialized = true;
        }
    }
}

class PluginPreferences : IPluginPreferences
{
    Preferences prefs;
    string plugin_name;
    this(Preferences prefs, string plugin_name)
    {
        this.prefs = prefs;
        this.plugin_name = plugin_name;
    }

    public void plugin_preferences(JSONValue plugin_prefs)
    {
        this.prefs.plugin_preferences(this.plugin_name, plugin_prefs);
    }

    public JSONValue plugin_preferences()
    {
        return this.prefs.plugin_preferences(this.plugin_name);
    }

}
