// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.client.overview;

import std.conv;
import std.datetime;
import std.format;
import std.stdio;

public import gtk.Builder;
public import gtk.Calendar;
public import gtk.CellRenderer;
public import gtk.CellRendererText;
public import gtk.Label;
public import gtk.TreeIter;
public import gtk.TreeModel;
public import gtk.TreeStore;
public import gtk.TreeView;
public import gtk.TreeViewColumn;
public import gtk.Window;

public import ttracker.lib;

/++
    Show a overview dialog with 2 adjustable calendar widgets
+/
class Overview
{
    private
    {
        Calendar from_calendar;
        Calendar to_calendar;
        SysTime from_date;
        SysTime to_date;
        Label total_for_period;
        TreeStore treestore;
        TreeView treeview;
        ITTrackerDb db;
        Window window;
        enum COLUMNS = [GType.STRING, GType.STRING, GType.STRING];
    }
    public this(Builder builder, ITTrackerDb db)
    {
        window = cast(Window) builder.getObject("overview_window");
        from_calendar = cast(Calendar) builder.getObject("from_calendar");
        to_calendar = cast(Calendar) builder.getObject("to_calendar");
        total_for_period = cast(Label) builder.getObject("total_for_period");
        treestore = new TreeStore(COLUMNS);
        treeview = cast(TreeView) builder.getObject("overview_treeview");
        treeview.setModel(treestore);
        auto name_renderer = new CellRendererText();
        auto name_column = new TreeViewColumn("Name", name_renderer, "text", 0);
        auto start_column = new TreeViewColumn("Start", new CellRendererText(), "text",
            1);
        auto end_column = new TreeViewColumn("End", name_renderer, "text", 2);
        name_column.setCellDataFunc(name_renderer, &myDataFunc, cast(void*) 0, null);
        end_column.setCellDataFunc(name_renderer, &myDataFunc, cast(void*) 2, null);
        treeview.appendColumn(name_column);
        treeview.appendColumn(start_column);
        treeview.appendColumn(end_column);

        this.db = db;
        window.addOnDelete(delegate(Event, Widget) { hide(); return true; });

        from_calendar.addOnDaySelected(delegate(Calendar) {
            uint year, month, day;
            from_calendar.getDate(year, month, day);
            from_date = SysTime(DateTime(year, month + 1, day));
            refreshTreeview();
        });
        to_calendar.addOnDaySelected(delegate(Calendar) {
            uint year, month, day;
            to_calendar.getDate(year, month, day);
            to_date = SysTime(DateTime(year, month + 1, day));
            to_date += dur!"days"(1);
            refreshTreeview();
        });
    }

    extern (C) static void myDataFunc(GtkTreeViewColumn* tree_column,
        GtkCellRenderer* cell, GtkTreeModel* tree_model, GtkTreeIter* tree_iter, void* data)
    {
        CellRenderer renderer = new CellRenderer(cell);
        TreeModel model = new TreeModel(tree_model);
        TreeIter iter = new TreeIter(tree_iter);
        iter.setModel(model);
        if (iter.getParent() is null)
        {
            int index = cast(int) data;
            string text = model.getValue(iter, index).getString();
            renderer.setProperty("markup", format("<b>%s</b>", text));
        }
    }

    public void show()
    {
        auto today = Clock.currTime();
        to_date = SysTime(DateTime(today.year, today.month, today.day));
        from_date = SysTime(DateTime(today.year, today.month, 1));
        to_calendar.selectMonth(today.month - 1, today.year);
        to_calendar.selectDay(today.day);
        from_calendar.selectMonth(today.month - 1, today.year);
        from_calendar.selectDay(1);
        refreshTreeview();
        window.showAll();
    }

    public void hide()
    {
        window.hide();
    }

    private void refreshTreeview()
    {
        treestore.clear();
        TreeIter parent_iter;
        auto cur_day = DateTime(1970, 1, 1);
        size_t duration = 0;
        size_t total_duration = 0;
        immutable auto now = Clock.currTime().toUnixTime();
        foreach (ref a; db.getActivities().filter(
                "start >= " ~ to!string(from_date.toUnixTime()) ~ " and start <= " ~ to!string(
                to_date.toUnixTime())).orderBy("start desc"))
        {
            auto activity_start = cast(DateTime) SysTime.fromUnixTime(a.start);
            auto activity_end = cast(DateTime) SysTime.fromUnixTime(a.end);
            if (activity_start.day != cur_day.day)
            {
                if (duration != 0 && parent_iter !is null)
                {
                    treestore.setValue(parent_iter, 2, format("%.2fh", duration / 3600.0));
                    total_duration += duration;
                    duration = 0;
                }
                cur_day = activity_start;
                parent_iter = treestore.createIter();
                treestore.setValue(parent_iter, 0, to!string(cur_day.date()));
            }
            auto iter = treestore.createIter(parent_iter);
            treestore.setValue(iter, 0, a.name);
            treestore.setValue(iter, 1, format("%.2d:%.2d",
                activity_start.hour, activity_start.minute));
            if (a.end == 0)
            {
                duration += now - a.start;
                treestore.setValue(iter, 2, "In progress");
            }
            else
            {
                treestore.setValue(iter, 2, format("%.2d:%.2d (%.2fh)",
                    activity_end.hour, activity_end.minute, (a.end - a.start) / 3600.0));
                duration += a.end - a.start;
            }
        }

        if (duration != 0 && parent_iter !is null)
        {
            total_duration += duration;
            treestore.setValue(parent_iter, 2, format("%.2fh", duration / 3600.0));
            duration = 0;
        }
        total_for_period.setText(format("%.2fh", total_duration / 3600.0));
        treeview.expandAll();
    }
}
