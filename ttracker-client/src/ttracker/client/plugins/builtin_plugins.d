module ttracker.client.plugins.builtin_plugins;

private import std.json;
private import std.stdio;
private import std.conv;
import gtk.AboutDialog;
private import ttracker.lib.plugins;

class JSONReporter : IReportPlugin
{
    private Pixbuf icon;
    this()
    {
        icon = get_pixbuf_from_data(cast(immutable ubyte[]) import("hamster-applet.png"));
    }

    /// Ditto
    override string get_name() const
    {
        return "JSON";
    }

    override void about(Window parent)
    {
        with (new AboutDialog())
        {
            string[] names;
            names ~= "Jean-Baptiste Lab (jeanbaptiste.lab@gmail.com)";
            setAuthors(names);
            setComments("TTracker JSON Exporter plugin");
            setCopyright("© Jean-Baptiste Lab (jeanbaptiste.lab@gmail.com)");
            setLicenseType(GtkLicense.MIT_X11);
            setVersion("0.1");
            setLogo(icon);
            setTransientFor(parent);
            setWebsite("https://bitbucket.org/jbl/ttracker");
            setWebsiteLabel("TTracker's page on BitBucket");
            run();
            hide();
        }
    }

    override bool has_preferences()
    {
        return false;
    }

    override void preferences(Window parent, IPluginPreferences prefs)
    {
    }

    JSONValue toJSON(ref Activity a)
    {
        JSONValue result;
        result["id"] = JSONValue(a.id);
        result["uuid"] = JSONValue(a.uuid);
        result["low_name"] = JSONValue(a.low_name);
        result["name"] = JSONValue(a.name);
        result["description"] = JSONValue(a.description);
        result["start"] = JSONValue(a.start);
        result["end"] = JSONValue(a.end);
        return result;
    }

    override void report(ref ITTrackerDb db, string output, Date start, Date end)
    {
        JSONValue[] array;
        JSONValue activities = array;
        auto start_str = to!string((cast(SysTime)(start)).toUnixTime());
        auto end_str = to!string((cast(SysTime)(end)).toUnixTime());
        auto selection =  "start >= " ~ start_str ~ " and start <= " ~ end_str;
        foreach (a; db.getActivities().filter(selection).orderBy("start asc"))
        {
            activities.array ~= toJSON(a);
        }
        writeln(activities.toPrettyString());
    }
}


class JSONPluginDescriptor : IPluginDescriptor {
    private {
        JSONReporter instance;
        const (PluginDescription) DESCRIPTOR = {"JSON Reporter", "report"};
    }

    PluginDescription get_description() const {
        return DESCRIPTOR;
    }
    IPlugin get_instance() {
        if(instance is null) {
            instance = new JSONReporter();
        }
        return instance;
    }
}

private const IPluginDescriptor descriptor = new JSONPluginDescriptor();

version(plugins_as_dll) {
    extern (C) const(IPluginDescriptor) get_descriptor() {
        return descriptor;
    }
} else {
    import ttracker.lib.builtin_plugins : builtin_plugins, register_static_plugin;
    static this() {
        register_static_plugin(new JSONPluginDescriptor());
    }
}
