// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.client.plugins.manager;

import std.algorithm;
import std.experimental.logger;
import std.conv;

import derelict.util.sharedlib;

public import ttracker.client.preferences;
import ttracker.lib.plugins;
import ttracker.lib.builtin_plugins;
import build_config;

class PluginManager
{
    private
    {
        IPlugin[][string] categories;
        Preferences prefs;
        static immutable PLUGIN_CATEGORIES = ["report", "sync"];
        enum SYSTEM_PLUGIN_DIR = PLUGIN_DIR;

        IPlugin load_plugin(string plugin_lib, string category)
        {
            IPlugin res;
            try
            {
                info("Trying to load plugin: " ~ plugin_lib ~ "...");
                SharedLib plugin;
                plugin.load([plugin_lib]);
                IPluginDescriptor function() get_descriptor = cast(IPluginDescriptor function()) plugin.loadSymbol(
                    "get_descriptor");
                IPluginDescriptor d = get_descriptor();
                if (d !is null)
                {
                    if (d.get_description().category == category)
                    {
                        res = d.get_instance();
                    }
                }
                info("OK");
            }
            catch (Exception e)
            {
                warning("FAILED: " ~ to!string(e));
            }
            return res;
        }

        IPlugin[] get_plugins(string plugin_dir, string category)
        {
            IPlugin[] result = [];
            if (exists(plugin_dir))
            {
                foreach (string name; dirEntries(plugin_dir, SpanMode.shallow))
                {
                    string fullPath = buildPath(plugin_dir, name);
                    if (isFile(fullPath))
                    {
                        IPlugin p = this.load_plugin(fullPath, category);
                        if (p !is null)
                        {
                            result ~= p;
                        }
                    }
                }
            }
            return result;
        }

        void load_category(string category)
        {
            string category_dir = buildPath(SYSTEM_PLUGIN_DIR, "");
            info("Category: " ~ category ~ ", plugin dir:" ~ category_dir);
            this.categories[category] ~= this.get_plugins(category_dir, category);
        }

        void load_categories()
        {
            foreach (ref category; this.PLUGIN_CATEGORIES)
            {
                info("Loading plugins for category: " ~ category);
                this.load_category(category);
            }
            info(this.categories);
        }
    }

    this(ref Preferences prefs)
    {
        this.prefs = prefs;
        foreach (ref description; builtin_plugins)
        {
            this.categories[description.get_description().category] ~= description.get_instance();
        }
        this.load_categories();
    }

    ref IPlugin[][string] getCategories()
    {
        return categories;
    }

    void runSyncPlugins(ref ITTrackerDb db)
    {
        auto sync_plugins = filter!(p => prefs.plugin_enabled(p.get_name()))(
            cast(ISyncPlugin[]) this.categories["sync"]);
        foreach (ref p; sync_plugins)
        {
            PluginPreferences plugin_prefs = new PluginPreferences(prefs, p.get_name());
            p.sync(db, plugin_prefs);
        }
    }
}
