// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.app;

import std.conv;
import std.datetime;
import std.format;
import std.path;
import std.stdio;
import std.string;
import std.uni;
import std.experimental.logger;

import docopt;

import ddbc.core;
import ddbc.common;
import ddbc.drivers.sqliteddbc;

import build_config;
public import ttracker.lib;
public import ttracker.lib.db_impl;
public import ttracker.client.activitylist;
public import ttracker.client.dialogs;
public import ttracker.client.overview;
import ttracker.client.plugins.manager;
public import ttracker.client.preferences;

import gdk.Event;
import gdk.Keysyms;
import gdk.Pixbuf;
import gdkpixbuf.PixbufLoader;
import gio.Application : GioApplication = Application;
import gio.SimpleAction;
import glib.Bytes;
import glib.Timeout;
import glib.Util;
import glib.Variant;
import glib.VariantType;
import gtk.AboutDialog;
import gtk.AccelGroup;
import gtk.Action;
import gtk.ActionGroup;
import gtk.Application : Application;
import gtk.ApplicationWindow : ApplicationWindow;
import gtk.Builder;
import gtk.Button;
import gtk.CheckButton;
import gtk.Dialog;
import gtk.Entry;
import gtk.EntryCompletion;
import gtk.Label;
import gtk.ListStore;
import gtk.Main;
import gtk.Menu;
import gtk.MenuBar;
import gtk.MenuItem;
import gtk.MessageDialog;
import gtk.SeparatorMenuItem;
import gtk.StatusIcon;
import gtk.Tooltip;
import gtk.TreeIter;
import gtk.TreePath;
import gtk.TreeView;
import gtk.TreeViewColumn;
import gtk.Widget;
import gtk.Window;
import gtkc.giotypes : GApplicationFlags;

extern (C) char* bindtextdomain(const char* domain, const char* dir);
extern (C) void textdomain(const char* domain);
extern (C) void bind_textdomain_codeset(const char* domain, const char* encoding);
extern (C) char* gettext(immutable(char*) domain);

class TTrackerWindow
{
    private {
        Action edit_activity;
        Action remove_activity;
        Action start_tracking;
        Action stop_tracking;
        Action activity_overview;
        Action previous_day;
        Action next_day;
        Action today_action;
        Action add_earlier_activity;
        Activity* current_activity_;
        Activity activity;
        ActivityListStore activity_store;
        ActivityTreeView activity_treeview;
        Application app;
        ApplicationWindow window;
        Builder builder;
        EditActivityDialog update_activity_dialog;
        Entry activity_entry;
        Entry description_entry;
        Label current_activity_duration_label;
        Label current_activity_label;
        Label total_today_label;
        Label displayed_day;
        Menu menu;
        MenuItem configure_item;
        MenuItem start_tracking_item;
        MenuItem stop_tracking_item;
        Overview overview;
        Pixbuf app_icon;
        private immutable app_icon_data = cast(immutable ubyte[]) import("hamster-applet.png");
        PluginManager plugin_manager;
        Preferences preferences = new Preferences();
        PreferencesDialog preferences_dialog;
        ReportDialog report_dialog;
        StatusIcon status_icon;
        ITTrackerDb db;
        Timeout time_info_update_timeout;
        SysTime current_day;
        enum REFRESH_RATE = 10 * 1000;
    }

    this(Application application)
    {
        current_day = Clock.currTime();
        app = application;
        builder = new Builder();
        // TODO: this function is missing in my GTK3 version
        // builder.setApplication(app);
        if (!builder.addFromString(import("ttracker.glade")))
        {
            writefln("Oops, could not create Glade object, check your glade file ;)");
        }
        window = cast(ApplicationWindow) builder.getObject("ttracker_window");
        window.setApplication(app);
        window.setSizeRequest(640, 300);
        app_icon = get_pixbuf_from_data(app_icon_data);
        window.setIcon(app_icon);
        initUI();
    }

    @property protected void current_activity(Activity* a)
    {
        this.current_activity_ = a;
        stop_tracking.setSensitive(this.current_activity_ !is null);
        if (this.current_activity_ !is null)
        {
            if (time_info_update_timeout is null)
            {
                time_info_update_timeout = new Timeout(TTrackerWindow.REFRESH_RATE,
                    &updateTimeInformation);
            }
            current_activity_label.setText(activity.name);
            auto current_duration = Clock.currTime().toUnixTime() - this.current_activity_.start;
            current_activity_duration_label.setText(format("%.1fh",
                cast(float)(current_duration / 3600.0)));
        }
        else
        {
            if (time_info_update_timeout !is null)
            {
                time_info_update_timeout.stop();
                time_info_update_timeout = null;
            }
            current_activity_label.setText(_("No activity."));
            current_activity_duration_label.setText("");
        }
        updateStatusIcon();
    }

    @property protected Activity* current_activity()
    {
        return this.current_activity_;
    }

    private bool updateTimeInformation()
    {
        current_activity = current_activity;
        long duration = 0;

        refreshActivityList(delegate(Activity a) {
            if (a.end != 0)
            {
                duration += a.end - a.start;
            }
            else
            {
                auto current_duration = Clock.currTime().toUnixTime() - a.start;
                duration += current_duration;
                activity_store.updateCurrentActivity(a);
            }
        });
        // Let the sync plugins do their thing
        plugin_manager.runSyncPlugins(db);
        if (current_activity !is null) {
            activity = db.getActivity(current_activity.id);
            current_activity = &activity;
        }
        total_today_label.setText(format("%.1fh", cast(float)(duration / 3600.0)));
        return true;
    }

    private void addActivityFromUi()
    {
        activity = Activity();
        activity.name = activity_entry.getText();
        activity.low_name = toLower(activity_entry.getText());
        activity.description = description_entry.getText();
        activity.start = Clock.currTime().toUnixTime();
        activity.synced = 0;
        activity.changed = 1;
        db.addActivity(activity);
        current_activity = &activity;
    }

    private void updateStatusIcon()
    {
        auto tooltip = "No activity";
        if (current_activity !is null)
        {
            auto end = Clock.currTime().toUnixTime();
            if (current_activity.end != 0)
            {
                end = current_activity.end;
            }
            auto duration = (end - activity.start) / 3600.0;
            tooltip = format("%s - %.2f h", current_activity.name, duration);
        }
        status_icon.setTooltipText(tooltip);
    }

    private void refreshActivityList(void delegate(Activity) refreshDelegate = null)
    {
        activity_store.clear();
        auto start = get_midnight(current_day);
        auto end = get_before_day_after(current_day);
        auto filter = "start >= " ~ to!string(start.toUnixTime()) ~ " and start <= " ~ to!string(
            end.toUnixTime());
        Activity[] activities;
        foreach (ref a; db.getActivities().filter(filter).orderBy("start desc"))
        {
            activity_store.addActivity(a);
            activities ~= a;
        }
        if (refreshDelegate !is null)
        {
            foreach (ref a; activities)
            {
                refreshDelegate(a);
            }
        }
        updateStatusIcon();
    }

    private void makePopup()
    {
        ActionGroup popup_actions = cast(ActionGroup) builder.getObject("popup_actions");
        start_tracking = popup_actions.getAction("start_tracking");
        stop_tracking = popup_actions.getAction("stop_tracking");
        activity_overview = popup_actions.getAction("activity_overview");
        previous_day = popup_actions.getAction("previous_day");
        next_day = popup_actions.getAction("next_day");
        today_action = popup_actions.getAction("today");
        Action configure = popup_actions.getAction("configure");
        Action quit = popup_actions.getAction("quit");
        Action user_guide = popup_actions.getAction("user_guide");
        Action about = popup_actions.getAction("about");
        Action report = popup_actions.getAction("report");
        edit_activity = popup_actions.getAction("edit_activity");
        add_earlier_activity = popup_actions.getAction("add_earlier_activity");
        remove_activity = popup_actions.getAction("remove_activity");
        overview = new Overview(builder, db);

        start_tracking.addOnActivate(delegate(Action) {
            if (current_activity !is null)
            {
                current_activity.end = Clock.currTime().toUnixTime();
                current_activity.changed = 1;
                db.updateActivity(*current_activity);
            }
            addActivityFromUi();
            refreshActivityList();
            start_tracking.setSensitive(false);
            activity_entry.setText("");
            description_entry.setText("");
            // Let the sync plugins do their thing
            plugin_manager.runSyncPlugins(db);
            if (current_activity !is null) {
                activity = db.getActivity(current_activity.id);
                current_activity = &activity;
            }
        });
        stop_tracking.addOnActivate(delegate(Action) {
            activity.end = Clock.currTime().toUnixTime();
            activity.changed = 1;
            db.updateActivity(activity);
            current_activity = null;
            refreshActivityList();
            start_tracking.setSensitive(activity_entry.getText().length > 0);
            // Let the sync plugins do their thing
            plugin_manager.runSyncPlugins(db);
            if (current_activity !is null) {
                activity = db.getActivity(current_activity.id);
                current_activity = &activity;
            }
        });
        configure.addOnActivate(delegate(Action) { onConfigure(null); });

        remove_activity.addOnActivate(delegate(Action) {
            auto selection = activity_treeview.tv.getSelectedIter();
            if (selection !is null)
            {
                auto id = activity_store.getId(selection);
                Activity delete_me = db.getActivity(id);
                db.deleteActivity(delete_me);
                if (current_activity !is null)
                {
                    if (current_activity.id == delete_me.id)
                    {
                        current_activity = null;
                    }
                }
                long duration = 0;
                refreshActivityList(delegate(Activity a) {
                    if (a.end != 0)
                    {
                        duration += a.end - a.start;
                    }
                    else
                    {
                        auto current_duration = Clock.currTime().toUnixTime() - a.start;
                        duration += current_duration;
                        activity_store.updateCurrentActivity(a);
                    }
                });
                total_today_label.setText(format("%.1fh", cast(float)(duration / 3600.0)));
            }
        });

        add_earlier_activity.addOnActivate(delegate(Action) { newActivity(); });

        edit_activity.addOnActivate(delegate(Action) {
            auto selection = activity_treeview.tv.getSelectedIter();
            if (selection !is null)
            {
                auto id = activity_store.getId(selection);
                editActivity(id);
            }
        });

        quit.addOnActivate(delegate(Action) { app.quit(); });

        user_guide.addOnActivate(delegate(Action) {
            writeln("There will be a guide, sure.");
        });

        about.addOnActivate(delegate(Action) {
            with (new AboutDialog())
            {
                string[] names;
                names ~= "Jean-Baptiste Lab (jeanbaptiste.lab@gmail.com)";
                setAuthors(names);
                setComments("TTracker - clone(ish) of Hamster applet written in the D language");
                setCopyright("© Jean-Baptiste Lab (jeanbaptiste.lab@gmail.com)");
                setLicenseType(GtkLicense.MIT_X11);
                setVersion(TTRACKER_VERSION);
                setLogo(app_icon);
                setTransientFor(window);
                setWebsite("https://bitbucket.org/jbl/ttracker");
                setWebsiteLabel("TTracker's page on BitBucket");
                run();
                hide();
            }
        });
        activity_overview.addOnActivate(delegate(Action) { overview.show(); });
        report.addOnActivate(&onReport);

        previous_day.addOnActivate(delegate(Action) {
            current_day = get_day_before(current_day);
            displayed_day.setText(format("%.2d-%.2d-%d", current_day.day,
                current_day.month, current_day.year));
            long duration = 0;
            refreshActivityList(delegate(Activity a) {
                if (a.end != 0)
                {
                    duration += a.end - a.start;
                }
                else
                {
                    auto current_duration = Clock.currTime().toUnixTime() - a.start;
                    duration += current_duration;
                    activity_store.updateCurrentActivity(a);
                }
            });
            total_today_label.setText(format("%.1fh", cast(float)(duration / 3600.0)));
            today_action.setSensitive(true);
            next_day.setSensitive(true);
        });
        next_day.addOnActivate(delegate(Action) {
            auto today = cast(Date) Clock.currTime();
            current_day = get_day_after(current_day);
            long duration = 0;
            refreshActivityList(delegate(Activity a) {
                if (a.end != 0)
                {
                    duration += a.end - a.start;
                }
                else
                {
                    auto current_duration = Clock.currTime().toUnixTime() - a.start;
                    duration += current_duration;
                    activity_store.updateCurrentActivity(a);
                }
            });
            total_today_label.setText(format("%.1fh", cast(float)(duration / 3600.0)));
            if (today == cast(Date) current_day)
            {
                next_day.setSensitive(false);
                today_action.setSensitive(false);
                displayed_day.setText(_("Today"));
            }
            else
            {
                displayed_day.setText(format("%.2d-%.2d-%d", current_day.day,
                    current_day.month, current_day.year));
            }
        });
        today_action.addOnActivate(delegate(Action) {
            current_day = Clock.currTime();
            long duration = 0;
            refreshActivityList(delegate(Activity a) {
                if (a.end != 0)
                {
                    duration += a.end - a.start;
                }
                else
                {
                    auto current_duration = Clock.currTime().toUnixTime() - a.start;
                    duration += current_duration;
                    activity_store.updateCurrentActivity(a);
                }
            });
            total_today_label.setText(format("%.1fh", cast(float)(duration / 3600.0)));
            next_day.setSensitive(false);
            today_action.setSensitive(false);
            displayed_day.setText(_("Today"));
        });

        start_tracking_item = start_tracking.createMenuItem();
        stop_tracking_item = stop_tracking.createMenuItem();
        auto separator_item = new SeparatorMenuItem();
        configure_item = configure.createMenuItem();
        menu = new Menu();
        auto close_item = quit.createMenuItem();
        menu.append(start_tracking_item);
        menu.append(stop_tracking_item);
        menu.append(separator_item);
        menu.append(configure_item);
        separator_item = new SeparatorMenuItem();
        menu.append(separator_item);
        menu.append(close_item);
        menu.showAll();
    }

    private void onReport(Action action)
    {
        if (report_dialog is null)
        {
            report_dialog = new ReportDialog(window, builder, plugin_manager);
        }

        immutable auto response = report_dialog.run();
        if (response == ResponseType.OK)
        {
            try
            {
                report_dialog.selected.report(db, "output_file", report_dialog.start, report_dialog.end);
            }
            catch (Exception e)
            {
                auto error_dlg = new MessageDialog(this.window,
                    DialogFlags.MODAL, MessageType.ERROR, ButtonsType.CLOSE,
                    "Error: " ~ to!string(e));
                error_dlg.run();
                error_dlg.hide();
            }
        }
        report_dialog.hide();
    }

    private void onConfigure(MenuItem item)
    {
        if (preferences_dialog is null)
        {
            preferences_dialog = new PreferencesDialog(window, builder,
                preferences, plugin_manager);
        }
        preferences_dialog.run();
        preferences_dialog.hide();
    }

    /**
   * Create and initialize the GTK widgets
   */
    private void initUI()
    {
        window.addOnDelete(delegate(Event, Widget) { window.hide(); return true; });
        string db_file = buildPath(get_user_config_subdir("ttracker"), "ttracker.sqlite");
        db = new TTrackerDb(new ConnectionPoolDataSourceImpl(new SQLITEDriver(), db_file));

        status_icon = new StatusIcon(app_icon);
        makePopup();
        status_icon.addOnPopupMenu(delegate void(uint a1, uint a2, StatusIcon si) {
            menu.popup(a1, a2);
        });
        status_icon.addOnActivate(delegate void(StatusIcon si) {
            if (!window.isVisible())
            {
                window.show();
            }
            else
            {
                window.hide();
            }
        });

        activity_entry = cast(Entry) builder.getObject("activity_entry");
        activity_entry.addOnChanged(delegate(EditableIF) {
            start_tracking.setSensitive(activity_entry.getText().length > 0);
        });
        activity_entry.addOnKeyRelease(delegate(Event e, Widget) {
            if (e.key().keyval == GdkKeysyms.GDK_Escape)
            {
                activity_entry.setText("");
            }
            return true;
        });

        description_entry = cast(Entry) builder.getObject("description_entry");

        setupAutocompletion();

        activity_treeview = new ActivityTreeView(cast(TreeView) builder.getObject("activity_list"));
        activity_treeview.tv.addOnRowActivated(&rowActivatedCallback);
        activity_treeview.tv.addOnCursorChanged(&cursorChangedCallBack);

        activity_store = new ActivityListStore();
        long duration;
        current_activity_label = cast(Label) builder.getObject("current_activity");
        current_activity_duration_label = cast(Label) builder.getObject("current_activity_duration");
        total_today_label = cast(Label) builder.getObject("total_today");
        displayed_day = cast(Label) builder.getObject("displayed_day");
        activity_treeview.setModel(activity_store);
        refreshActivityList(delegate(Activity a) {
            if (a.end != 0)
            {
                duration += a.end - a.start;
            }
            else
            {
                this.activity = a;
                current_activity = &this.activity;
                auto current_duration = Clock.currTime().toUnixTime() - a.start;
                duration += current_duration;
            }
        });
        total_today_label.setText(format("%.1fh", cast(float)(duration / 3600.0)));

        plugin_manager = new PluginManager(preferences);

        if (preferences.show_window_at_launch)
        {
            window.show();
        }
    }

    void setupAutocompletion()
    {
        auto activity_entry_completion = cast(EntryCompletion) builder.getObject(
            "activity_entry_completion");
        auto activity_entry_completion_liststore = new ListStore([GType.STRING]);
        activity_entry_completion.setModel(activity_entry_completion_liststore);
        activity_entry_completion.setTextColumn(0);
        foreach (ref a; db.getActivities().groupBy("name"))
        {
            auto iter = activity_entry_completion_liststore.createIter();
            activity_entry_completion_liststore.setValue(iter, 0, a.name);
        }
        activity_entry.setCompletion(activity_entry_completion);
        auto description_entry_completion = cast(EntryCompletion) builder.getObject(
            "description_entry_completion");
        auto description_entry_completion_liststore = new ListStore([GType.STRING]);
        description_entry_completion.setModel(description_entry_completion_liststore);
        description_entry_completion.setTextColumn(0);
        foreach (ref a; db.getActivities().groupBy("description"))
        {
            auto iter = description_entry_completion_liststore.createIter();
            description_entry_completion_liststore.setValue(iter, 0, a.description);
        }
        description_entry.setCompletion(description_entry_completion);
    }

    void cursorChangedCallBack(TreeView treeView)
    {
        auto selection = treeView.getSelectedIter();
        remove_activity.setSensitive(selection !is null);
        edit_activity.setSensitive(selection !is null);
        if (selection !is null)
        {
            auto value = to!string(treeView.getModel().getValue(selection, 1).getString());
        }
    }

    private ResponseType editActivity(long id)
    {
        auto res = ResponseType.OK;
        try
        {
            immutable Activity selected_activity = db.getActivity(id);
            bool close_dlg = false;
            do
            {
                if (update_activity_dialog is null)
                {
                    update_activity_dialog = new EditActivityDialog(window, builder, db,
                        res);
                }
                update_activity_dialog.activity = selected_activity;
                res = update_activity_dialog.run();
                switch (res)
                {
                case ResponseType.OK:
                    update_activity_dialog.hide();
                    update_activity_dialog.activity.changed = 1;
                    db.updateActivity(update_activity_dialog.activity);
                    activity_store.setInfo(update_activity_dialog.activity);
                    if (current_activity !is null)
                    {
                        if (update_activity_dialog.activity.id == current_activity.id)
                        {
                            if (update_activity_dialog.activity.end != 0)
                            {
                                current_activity = null;
                            }
                        }
                        else
                        {
                            if (update_activity_dialog.activity.end == 0)
                            {
                                current_activity.end = Clock.currTime().toUnixTime();
                                current_activity.changed = 1;
                                db.updateActivity(*current_activity);
                            }
                        }
                    }

                    if (update_activity_dialog.activity.end == 0)
                    {
                        activity = update_activity_dialog.activity;
                        current_activity = &activity;
                    }
                    close_dlg = true;
                    updateTimeInformation();
                    break;
                case ResponseType.REJECT:
                    update_activity_dialog.hide();
                    break;
                case ResponseType.CANCEL:
                case ResponseType.DELETE_EVENT:
                    close_dlg = true;
                    break;
                default:
                    break;
                }
            }
            while (!close_dlg);
            update_activity_dialog.hide();
        }
        catch (Exception e)
        {
            warning("Exception from getActivity(" ~ to!string(id) ~ "):" ~ to!string(e));
        }
        return res;
    }

    private ResponseType newActivity()
    {
        auto res = ResponseType.OK;
        try
        {
            Activity selected_activity = Activity();
            selected_activity.start = Clock.currTime().toUnixTime();
            selected_activity.name = _("Earlier activity");
            bool close_dlg = false;
            do
            {
                if (update_activity_dialog is null)
                {
                    update_activity_dialog = new EditActivityDialog(window, builder, db,
                        res);
                }
                update_activity_dialog.activity = selected_activity;
                res = update_activity_dialog.run();
                switch (res)
                {
                case ResponseType.OK:
                    update_activity_dialog.hide();
                    db.addActivity(update_activity_dialog.activity);
                    activity_store.addActivity(update_activity_dialog.activity);
                    if (current_activity !is null)
                    {
                        if (update_activity_dialog.activity.id == current_activity.id)
                        {
                            if (update_activity_dialog.activity.end != 0)
                            {
                                current_activity = null;
                            }
                        }
                        else
                        {
                            if (update_activity_dialog.activity.end == 0)
                            {
                                current_activity.end = Clock.currTime().toUnixTime();
                                current_activity.changed = 1;
                                db.updateActivity(*current_activity);
                            }
                        }
                    }

                    if (update_activity_dialog.activity.end == 0)
                    {
                        activity = update_activity_dialog.activity;
                        current_activity = &activity;
                    }
                    close_dlg = true;
                    break;
                case ResponseType.REJECT:
                    update_activity_dialog.hide();
                    break;
                case ResponseType.CANCEL:
                case ResponseType.DELETE_EVENT:
                    close_dlg = true;
                    break;
                default:
                    break;
                }
            }
            while (!close_dlg);
            update_activity_dialog.hide();
        }
        catch (Exception e)
        {
            warning("Exception:" ~ to!string(e));
        }
        return res;
    }

    void rowActivatedCallback(TreePath path, TreeViewColumn column, TreeView treeView)
    {
        edit_activity.activate();
    }
}

class TTrackerApp : Application
{
    TTrackerWindow window;
    this(string applicationId, GApplicationFlags flags)
    {
        super(applicationId, flags);

        SimpleAction action = new SimpleAction("simple-action", new VariantType("s"));
        action.addOnActivate(delegate(Variant v, SimpleAction) {
            size_t s;
            auto arg = v.getString(s);
            writeln(arg);
        });
        addAction(action);
        addOnActivate(delegate void(GioApplication app) {
            if (window is null)
            {
                window = new TTrackerWindow(this);
            }
        });
    }

    public override void quit() {
        if (window.preferences.close_active_task_on_exit) {
            window.stop_tracking.activate();
        }
        super.quit();
    }
}

extern (C) int daemon(int nochdir, int noclose);

int main(string[] args)
{
    version (posix) {
        import core.stdc.locale : setlocale, LC_ALL;
        setlocale(LC_ALL, "");
        bindtextdomain(PACKAGE_NAME, LOCALE_DIR);
        bind_textdomain_codeset(PACKAGE_NAME, "UTF-8");
        textdomain(PACKAGE_NAME);
    }
    immutable auto log_file = buildPath(get_user_config_subdir("ttracker"), "ttracker.log");
    sharedLog = new FileLoggerWithLevel(log_file);
    auto application = new TTrackerApp("jbl.gtkd.TTracker", GApplicationFlags.FLAGS_NONE);
    auto doc = _("TTracker. A small time tracking application, inspired by Hamster.

    Usage:
      ttracker add <activity> [-s START | --start=START] [-d DESC | --description=DESC] [-e END | --end=END]
      ttracker [--daemon]
      ttracker -h | --help
      ttracker --version

    Options:
      -h --help                     Show this screen.
      --version                     Show version.
      --daemon                      Daemonize ttracker so it survives closing the shell it was starting from (Posix only)
      -s START --start=START        Start date/time of activity
      -e END --end=END              End date/time of activity
      -d DESC --description=DESC    Activity description
    ");

    auto arguments = docopt.docopt(doc, args[1 .. $], true, "ttracker 0.1");
    if (arguments["--daemon"].isTrue())
    {
        version (Posix)
        {
            daemon(1, 1);
        }
    }
    else if (arguments["add"].isTrue())
    {
        application.register(null);
        application.activateAction("simple-action", new Variant(arguments["<activity>"].toString()));
    }

    string[] empty;
    return application.run(empty);
}
