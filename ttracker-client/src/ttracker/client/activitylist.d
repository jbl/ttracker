// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.client.activitylist;

private import ttracker.lib.models;

import std.conv;
import std.format;
import std.stdio;
import std.datetime;
private import gobject.Value;
private import gtk.ListStore;
private import gdk.Pixbuf;
private import gtk.TreeIter;
private import gtk.TreePath;
private import gtk.TreeView;
private import gtk.TreeViewColumn;
private import gtk.CellRendererText;
private import gtk.CellRendererPixbuf;
private import gtkc.gobjecttypes;

private static immutable enum COLUMN_IDX {
  START,
  NAME,
  DESC,
  DURATION,
  ID
}

class ActivityListStore : ListStore
{
  static immutable COLUMNS = [GType.STRING, GType.STRING, GType.STRING, GType.STRING, GType.LONG];
  TreeIter active_iter;
  this() {
    super(COLUMNS.dup);
  }

  private void setInfo(TreeIter iter, in Activity activity) {
    long end;
    string start_str;
    setValue(iter, COLUMN_IDX.NAME, activity.name);
    setValue(iter, COLUMN_IDX.DESC, "-  " ~ activity.description);

    if (activity.start == 0){
      setValue(iter, COLUMN_IDX.START, "");
    } else {
      auto start_dt = SysTime.fromUnixTime(activity.start);
      start_str = format("%.2d:%.2d - ", start_dt.hour, start_dt.minute);
    }
    if (activity.end == 0){
      end = Clock.currTime().toUnixTime();
      active_iter = iter;
    } else {
      end = activity.end;
      auto end_dt = SysTime.fromUnixTime(activity.end);
      start_str ~= format("%.2d:%.2d", end_dt.hour, end_dt.minute);
    }
    setValue(iter, COLUMN_IDX.START, start_str);
    immutable auto delta = end - activity.start;
    auto hours = delta / 3600;
    auto minutes =  (delta - (hours * 3600)) / 60;
    auto duration_str = format("%dh %dmin", hours, minutes);
    setValue(iter, COLUMN_IDX.DURATION, duration_str);
    setValue(iter, COLUMN_IDX.ID, cast(int)activity.id);
  }

  public void addActivity(in Activity activity) {
    TreeIter iter = createIter();
    setInfo(iter, activity);
  }

  public void updateCurrentActivity(in Activity activity) {
    if (active_iter !is null) {
      setInfo(active_iter, activity);
    }
  }

  public long getId(TreePath path) {
    TreeIter iter = new TreeIter();
    getIter(iter, path);
    auto value = getValue(iter, COLUMN_IDX.ID);
    return value.getLong();
  }

  public long getId(TreeIter iter) {
    auto value = getValue(iter, COLUMN_IDX.ID);
    return value.getLong();
  }

  public void setInfo(TreePath path, in Activity activity) {
    TreeIter iter = new TreeIter();
    getIter(iter, path);
    setInfo(iter, activity);
  }

  public void setInfo(in Activity activity) {
    TreeIter iter;
    if (getIterFirst(iter)) {
      Value value;
      do {
        value = getValue(iter, COLUMN_IDX.ID);
        if(value.getLong() == activity.id) {
          setInfo(iter, activity);
          break;
        }
      } while (iterNext(iter));
    }
  }

  public override void clear() {
    super.clear();
    active_iter = null;
  }
}

class ActivityTreeView {
  public TreeView tv;
  private TreeViewColumn name_column;
  private TreeViewColumn description_column;
  private TreeViewColumn start_column;
  private TreeViewColumn duration_column;

  this(TreeView tv) {
    this.tv = tv;
    name_column = new TreeViewColumn("Name", new CellRendererText(), "text", COLUMN_IDX.NAME);
    description_column = new TreeViewColumn("Description", new CellRendererText(), "text", COLUMN_IDX.DESC);
    start_column = new TreeViewColumn("Start", new CellRendererText(), "text", COLUMN_IDX.START);
    duration_column = new TreeViewColumn("End", new CellRendererText(), "text", COLUMN_IDX.DURATION);
    this.tv.appendColumn(start_column);
    this.tv.appendColumn(name_column);
    this.tv.appendColumn(description_column);
    this.tv.appendColumn(duration_column);
  }

  void setModel(ListStore model) {
    this.tv.setModel(model);
  }
}
