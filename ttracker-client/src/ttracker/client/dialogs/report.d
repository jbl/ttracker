// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.client.dialogs.report;

private import std.conv;
private import std.datetime;
private import std.format;
private import std.stdio;
private import std.uni;

private import gtk.Builder;
private import gtk.ComboBoxText;
private import gtk.Dialog;
private import gtk.Entry;
private import gtk.Grid;
private import gtk.Window;

import ttracker.lib.plugins;
import ttracker.client.plugins.manager;

class ReportDialog {

  Date start_;
  Date end_;
  int report_type_;
  ComboBoxText report_type_combo;
  Dialog dlg;
  Entry start_date;
  Entry start_time;
  Entry end_date;
  PluginManager plugin_manager;
  IReportPlugin selected;
  this(Window parent, Builder b, ref PluginManager plugin_manager) {
    StockID[] buttons = [StockID.OK, StockID.CANCEL];
    ResponseType[] responses = [ResponseType.OK, ResponseType.CANCEL];
    dlg = new Dialog("Report", parent, DialogFlags.MODAL|DialogFlags.DESTROY_WITH_PARENT, buttons, responses);
    auto dlg_content = cast(Grid)b.getObject("report_dialog_content_area");
    start_date = cast(Entry)b.getObject("report_start_date");
    end_date = cast(Entry)b.getObject("report_end_date");
    report_type_combo = cast(ComboBoxText)b.getObject("report_type_combo");
    dlg_content.reparent(dlg.getContentArea());
    start_ = cast(Date)Clock.currTime();
    start_.day = 1;
    end_ = cast(Date)Clock.currTime();
    this.plugin_manager = plugin_manager;
  }

  @property
  void start(Date s) {
    start_ = s;
  }

  @property
  Date start() {
    return start_;
  }

  @property
  void end(Date e) {
    end_ = e;
  }

  @property
  Date end() {
    return end_;
  }

  @property
  void report_type(int rt) {
    report_type_ = rt;
    report_type_combo.setActive(report_type_);
  }

  @property
  int report_type() {
    return report_type_;
  }

  public ResponseType run() {
    start_date.setText(format("%d-%.2d-%.2d", start_.year, start_.month, start_.day));
    end_date.setText(format("%d-%.2d-%.2d", end_.year, end_.month, end_.day));
    report_type_combo.removeAll();
    foreach(reporter; plugin_manager.getCategories()["report"]) {
      report_type_combo.appendText(reporter.get_name());
    }
    report_type_combo.setActive(0);
    auto res = cast(ResponseType)this.dlg.run();
    if (res == ResponseType.OK) {
        int year, month, day;
        string str = start_date.getText();
        formattedRead(str, "%d-%d-%d", &year, &month, &day);
        start_ = Date(year, month, day);
        str = end_date.getText();
        formattedRead(str, "%d-%d-%d", &year, &month, &day);
        end_ = Date(year, month, day);
        report_type_ = report_type_combo.getActive();
        if (report_type != -1) {
            selected = cast(IReportPlugin)plugin_manager.getCategories()["report"][report_type_];
        }
    }
    return res;
  }

  public void hide() {
    dlg.hide();
  }
}
