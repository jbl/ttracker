// TTracker - A Hamster applet clone(ish)
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.client.dialogs.edit;

private import std.conv;
private import std.datetime;
private import std.format;
private import std.stdio;
private import std.uni;

private import gdk.RGBA;
private import gtk.Builder;
private import gtk.CheckButton;
private import gtk.Dialog;
private import gtk.Entry;
private import gtk.EntryCompletion;
private import gtk.ListStore;
private import gtk.Grid;
private import gtk.Window;

private import ttracker.lib.models;
public import ttracker.lib.db;

class EditActivityDialog {
  Dialog dlg;
  Activity activity_;
  Entry name;
  Entry description;
  Entry start_date;
  Entry start_time;
  Entry end_date;
  Entry end_time;
  CheckButton in_progress;

  this(Window parent, Builder b, ITTrackerDb db, ResponseType previous=ResponseType.OK) {
    StockID[] buttons = [StockID.OK, StockID.CANCEL];
    ResponseType[] responses = [ResponseType.OK, ResponseType.CANCEL];
    dlg = new Dialog("Edit Activity", parent, DialogFlags.MODAL|DialogFlags.DESTROY_WITH_PARENT, buttons, responses);
    auto dlg_content = cast(Grid)b.getObject("edit_dialog_content_area");
    name = cast(Entry)b.getObject("dlg_activity_entry");
    description = cast(Entry)b.getObject("dlg_description_entry");
    start_date = cast(Entry)b.getObject("start_date");
    start_time = cast(Entry)b.getObject("start_time");
    end_date = cast(Entry)b.getObject("end_date");
    end_time = cast(Entry)b.getObject("end_time");
    in_progress = cast(CheckButton)b.getObject("in_progress");
    dlg_content.reparent(dlg.getContentArea());
    if (previous == ResponseType.OK) {
      start_date.overrideBackgroundColor(StateFlags.NORMAL, null);
      start_time.overrideBackgroundColor(StateFlags.NORMAL, null);
      end_date.overrideBackgroundColor(StateFlags.NORMAL, null);
      end_time.overrideBackgroundColor(StateFlags.NORMAL, null);
    }
    in_progress.addOnToggled(delegate(ToggleButton) {
        end_date.setSensitive(!in_progress.getActive());
        end_time.setSensitive(!in_progress.getActive());
      });
      setupAutoCompletion(b, db);
  }

  private void setupAutoCompletion(Builder builder, ITTrackerDb db) {
    auto activity_entry_completion = cast(EntryCompletion) builder.getObject("edit_activity_entry_completion");
    name.setCompletion(activity_entry_completion);
    auto activity_entry_completion_liststore = new ListStore([GType.STRING]);
    activity_entry_completion.setModel(activity_entry_completion_liststore);
    activity_entry_completion.setTextColumn(0);
    foreach (ref a; db.getActivities().groupBy("name"))
    {
        auto iter = activity_entry_completion_liststore.createIter();
        activity_entry_completion_liststore.setValue(iter, 0, a.name);
    }
    name.setCompletion(activity_entry_completion);
  }

  @property
  public void activity(Activity a) {
    this.activity_ = a;
    name.setText(activity.name);
    description.setText(activity.description.length?activity.description:"");
    auto start_dt = SysTime.fromUnixTime(activity.start);
    start_date.setText(format("%d-%.2d-%.2d", start_dt.year, start_dt.month, start_dt.day));
    start_time.setText(format("%.2d:%.2d", start_dt.hour, start_dt.minute));
    in_progress.setActive(activity.end == 0);
    end_date.setSensitive(activity.end != 0);
    end_time.setSensitive(activity.end != 0);
    SysTime end_dt;
    if(activity.end == 0) {
      end_dt = Clock.currTime();
    } else {
      end_dt = SysTime.fromUnixTime(activity.end);
    }
    end_date.setText(format("%d-%.2d-%.2d", end_dt.year, end_dt.month, end_dt.day));
    end_time.setText(format("%.2d:%.2d", end_dt.hour, end_dt.minute));
  }

  @property
  public ref Activity activity() {
    return this.activity_;
  }

  public ResponseType run() {
    auto res = cast(ResponseType)this.dlg.run();
    if (res == ResponseType.OK) {
      int year, month, day, hour, minute;
      string date_str = start_date.getText() ~ "-" ~ start_time.getText();
      try {
        formattedRead(date_str, "%d-%d-%d-%d:%d", &year, &month, &day, &hour, &minute);
        activity_.start = SysTime(DateTime(year, month, day, hour, minute, 0)).toUnixTime();
      } catch(ConvException e) {
        writeln(to!string(e));
        res = ResponseType.REJECT;
        auto colour = new RGBA(255, 0, 0);
        start_date.overrideBackgroundColor(StateFlags.NORMAL, colour);
        start_time.overrideBackgroundColor(StateFlags.NORMAL, colour);
      }
      if(in_progress.getActive()) {
        activity_.end = 0;
      } else {
        date_str = end_date.getText() ~ "-" ~ end_time.getText();
        try {
          formattedRead(date_str, "%d-%d-%d-%d:%d", &year, &month, &day, &hour, &minute);
          activity_.end = SysTime(DateTime(year, month, day, hour, minute, 0)).toUnixTime();
        } catch(ConvException e) {
          writeln(to!string(e));
          res = ResponseType.REJECT;
          auto colour = new RGBA(255, 0, 0);
          end_date.overrideBackgroundColor(StateFlags.NORMAL, colour);
          end_time.overrideBackgroundColor(StateFlags.NORMAL, colour);
        }
      }
      activity_.name = name.getText();
      activity_.description = description.getText();
      activity_.low_name = toLower(name.getText());
    }
    return res;
  }

  public void hide() {
    dlg.hide();
  }
}
