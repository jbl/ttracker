// TTracker
// Copyright (C) 2017  Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// Except as contained in this notice, the name(s) of the above copyright
// holders shall not be used in advertising or otherwise to promote the
// sale, use or other dealings in this Software without prior written
// authorization.

module ttracker.client.dialogs.preferences;

import gobject.ObjectG;
import gobject.Value;
import gtk.Builder;
import gtk.CheckButton;
import gtk.Dialog;
import gtk.Notebook;
import gtk.ToggleButton;
import gtk.Window;
import gtk.Button;
import gtk.CellRenderer;
import gtk.CellRendererText;
import gtk.CellRendererToggle;
import gtk.TreeIter;
import gtk.TreeModel;
import gtk.TreePath;
import gtk.TreeStore;
import gtk.TreeView;
import gtk.TreeViewColumn;

import std.conv;
import std.stdio;

import ttracker.lib.plugins;
import ttracker.client.plugins.manager;
import ttracker.client.preferences;

class PreferencesDialog
{
    private
    {
        Button about_plugin;
        Button preferences_plugin;
        PluginManager plugin_manager;
        Preferences preferences;
        Dialog dlg;
        TreeStore treestore;
        TreeView treeview;
        IPlugin selected_plugin;

        enum COLUMNS = [GType.STRING, GType.INT, GType.POINTER, GType.BOOLEAN];
    }

    public this(Window parent, Builder b, ref Preferences prefs, ref PluginManager plugin_manager)
    {
        StockID[] buttons = [StockID.CLOSE];
        ResponseType[] responses = [ResponseType.CLOSE];

        this.preferences = prefs;
        this.plugin_manager = plugin_manager;
        dlg = new Dialog("Preferences", parent,
            DialogFlags.MODAL | DialogFlags.DESTROY_WITH_PARENT, buttons, responses);
        dlg.setDefaultSize(400, 400);
        auto dlg_content = cast(Notebook) b.getObject("preferences_notebook");
        auto autostart = cast(CheckButton) b.getObject("auto_start");
        autostart.setActive(preferences.autostart);
        autostart.addOnToggled(delegate(ToggleButton b) {
            preferences.autostart = b.getActive();
        });

        auto show_window_at_startup = cast(CheckButton) b.getObject("show_window_at_startup");
        show_window_at_startup.setActive(preferences.show_window_at_launch);
        show_window_at_startup.addOnToggled(delegate(ToggleButton b) {
            preferences.show_window_at_launch = b.getActive();
        });

        auto close_active_task_on_exit = cast(CheckButton) b.getObject("close_active_task_on_exit");
        close_active_task_on_exit.setActive(preferences.close_active_task_on_exit);
        close_active_task_on_exit.addOnToggled(delegate(ToggleButton b) {
            preferences.close_active_task_on_exit = b.getActive();
        });

        treestore = new TreeStore(COLUMNS);
        treeview = cast(TreeView) b.getObject("plugins_treeview");
        treeview.setModel(treestore);
        auto name_renderer = new CellRendererText();
        auto name_column = new TreeViewColumn("Name", name_renderer, "text", 0);
        auto enabled_renderer = new CellRendererToggle();
        auto enabled_column = new TreeViewColumn("Enabled", enabled_renderer, "active",
            1);
        enabled_column.addAttribute(enabled_renderer, "visible", 3);
        enabled_renderer.addOnToggled(delegate void(string p, CellRendererToggle t) {
            auto path = new TreePath(p);
            auto it = new TreeIter(treestore, path);
            treestore.setValue(it, 1, it.getValueInt(1) ? 0 : 1);
            preferences.plugin_enabled(it.getValueString(0), it.getValueInt(1) ? true : false);
        });
        name_column.setCellDataFunc(name_renderer, &setBold, cast(void*) 0, null);
        // enabled_column.setCellDataFunc(enabled_renderer, &hideCheckBox, cast(void *)0, null);
        treeview.appendColumn(name_column);
        treeview.appendColumn(enabled_column);
        treeview.addOnCursorChanged(&cursorChangedCallBack);

        about_plugin = cast(Button) b.getObject("about_plugin");
        about_plugin.setSensitive(false);
        about_plugin.addOnClicked(delegate(Button) { selected_plugin.about(dlg); });

        preferences_plugin = cast(Button) b.getObject("preferences_plugin");
        preferences_plugin.setSensitive(false);
        preferences_plugin.addOnClicked(delegate(Button) {
            PluginPreferences plugin_prefs = new PluginPreferences(preferences,
                selected_plugin.get_name());
            selected_plugin.preferences(dlg, plugin_prefs);
        });

        refreshTreeview();

        dlg_content.reparent(dlg.getContentArea());
    }

    void cursorChangedCallBack(TreeView treeView)
    {
        auto selection = treeView.getSelectedIter();
        about_plugin.setSensitive(false);
        preferences_plugin.setSensitive(false);
        if (selection !is null)
        {
            selected_plugin = cast(IPlugin)(treeView.getModel().getValue(selection,
                2).getPointer());
            about_plugin.setSensitive(selected_plugin !is null);
            if (selected_plugin !is null)
            {
                preferences_plugin.setSensitive(selected_plugin.has_preferences());
            }
        }
    }

    private void refreshTreeview()
    {
        treestore.clear();
        TreeIter parent_iter;
        auto categories = plugin_manager.getCategories();
        foreach (ref c; categories.byKey)
        {
            parent_iter = treestore.createIter();
            treestore.setValue(parent_iter, 0, c);
            treestore.setValue(parent_iter, 3, false);
            foreach (ref p; categories[c])
            {
                auto iter = treestore.createIter(parent_iter);
                treestore.setValue(iter, 0, p.get_name());
                treestore.setValue(iter, 1, preferences.plugin_enabled(p.get_name()));
                Value value = new Value();
                value.init(GType.POINTER);
                value.setPointer(cast(void*) p);
                treestore.setValue(iter, 2, value);
                treestore.setValue(iter, 3, true);
            }
        }
    }

    extern (C) static void setBold(GtkTreeViewColumn* tree_column,
        GtkCellRenderer* cell, GtkTreeModel* tree_model, GtkTreeIter* tree_iter, void* data)
    {
        CellRenderer renderer = new CellRenderer(cell);
        TreeModel model = new TreeModel(tree_model);
        TreeIter iter = new TreeIter(tree_iter);
        iter.setModel(model);
        if (iter.getParent() is null)
        {
            int index = cast(int) data;
            string text = model.getValue(iter, index).getString();
            renderer.setProperty("markup", format("<b>%s</b>", text));
        }
    }

    extern (C) static void hideCheckBox(GtkTreeViewColumn* tree_column,
        GtkCellRenderer* cell, GtkTreeModel* tree_model, GtkTreeIter* tree_iter, void* data)
    {
        CellRenderer renderer = new CellRenderer(cell);
        TreeModel model = new TreeModel(tree_model);
        TreeIter iter = new TreeIter(tree_iter);
        iter.setModel(model);
        if (iter.getParent() is null)
        {
            renderer.setProperty("visible", false);
        }
        else
        {
            renderer.setProperty("visible", true);
        }
    }

    public int run()
    {
        return dlg.run();
    }

    public void hide()
    {
        dlg.hide();
    }
}
