import pkgutil, os
plugins = []
for loader, module_name, _ in pkgutil.iter_modules(plugins_paths):
    if module_name != 'test':
        mod = loader.find_module(module_name).load_module(module_name)
        if hasattr(mod, "get_plugin"):
            try:
                plugins.append(mod)
            except ImportError:
                pass
