# The MIT License (MIT)
# 
# Copyright (c) 2015 Jean-Baptiste Lab <jeanbaptiste.lab@gmail.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE. 

import datetime

class Plugin(object):
    def __init__(self):
        pass
    def get_name(self):
        return "Python reporter 2"
    def report(self, data):
        # print "Damn, that's some powerful shit'"
        for activity in data:
            start = datetime.datetime.fromtimestamp(activity.start).strftime('%Y-%m-%d %H:%M')
            if activity.end != 0:
                duration = activity.end - activity.start
                end = "%.1f hours"% (duration / 3600.0)
            else:
                end = "-"
            print start, end, ':\t', activity.name, activity.description

plugin = Plugin()

def get_plugin():
    return plugin
